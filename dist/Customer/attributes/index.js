var Sequelize = require('sequelize');
exports.attributes = {
    name: {
        type: Sequelize.STRING
    },
    mobile: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        validate: {
            isEmail: { msg: 'Not a Valid Email' }
        }
    }
};

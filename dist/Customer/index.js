var instanceMethods = require('./instanceMethods');
exports.instanceMethods = instanceMethods;
var classMethods = require('./ClassMethods');
exports.classMethods = classMethods;
var attributes = require('./Attributes');
exports.attributes = attributes;
var hooks = require('./Hooks');
exports.hooks = hooks;
var getterMethods = require('./GetterMethods');
exports.getterMethods = getterMethods;
var setterMethods = require('./SetterMethods');
exports.setterMethods = setterMethods;
var scopes = require('./Scopes');
exports.scopes = scopes;
var defaultScope = require('./DefaultScope');
exports.defaultScope = defaultScope;
exports.modelOptions = { instanceMethods: instanceMethods, classMethods: classMethods };
if (getterMethods) {
    exports.modelOptions['getterMethods'] = getterMethods;
}
if (setterMethods) {
    exports.modelOptions['setterMethods'] = setterMethods;
}
if (hooks) {
    exports.modelOptions['hooks'] = hooks;
}

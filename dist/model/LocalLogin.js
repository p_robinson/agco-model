var prUser_1 = require('prUser');
module.exports = function (sequelize, DataTypes) {
    sequelize.Utils._.extend(prUser_1.classMethods, {
        associate: function (models) {
            LocalLogin.belongsTo(models.Users);
        }
    });
    var LocalLogin = sequelize.define('LocalLogin', prUser_1.attributes.attributes, {
        instanceMethods: prUser_1.instanceMethods,
        classMethods: prUser_1.classMethods,
        hooks: prUser_1.hooks
    });
    return LocalLogin;
};

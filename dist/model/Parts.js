module.exports = function (sequelize, DataTypes) {
    var Parts = sequelize.define('Parts', {
        type: DataTypes.STRING,
        model: DataTypes.STRING,
        serial: DataTypes.STRING,
        installed: DataTypes.DATE,
        removed: DataTypes.DATE
    });
    return Parts;
};

module.exports = function (sequelize, DataTypes) {
    var SystemOwners = sequelize.define('SystemOwners', {
        sold: DataTypes.BOOLEAN
    });
    return SystemOwners;
};

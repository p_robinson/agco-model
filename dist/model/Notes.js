module.exports = function (sequelize, DataTypes) {
    var Notes = sequelize.define('Notes', {
        type: {
            type: DataTypes.ENUM,
            values: ['SYSTEMS', 'CUSTOMERS', 'SERVICES', 'COMMISSIONS', 'PARTS']
        },
        tableId: {
            type: DataTypes.UUID
        },
        note: {
            type: DataTypes.TEXT
        },
        user: {
            type: DataTypes.ENUM,
            values: ['AUTHORISED', 'OWNER']
        },
        attention: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    });
    return Notes;
};

var Xero = require('xero-extension');
module.exports = function (sequelize, DataTypes) {
    var XeroCustomers = sequelize.define('XeroCustomers', {
        ContactID: {
            type: DataTypes.STRING,
        },
        XeroProfileId: {
            type: DataTypes.UUID,
            references: {
                model: 'XeroOrganisations',
                key: 'OrganisationId'
            },
            allowNull: false
        },
        profile: {
            type: DataTypes.BLOB,
            get: function () {
                try {
                    var profile = JSON.parse(this.getDataValue('profile'));
                }
                catch (err) {
                    return;
                }
                return profile;
            },
            set: function (val) {
                this.setDataValue('profile', JSON.stringify(val));
            }
        },
        valid: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        lastSync: {
            type: DataTypes.DATE
        }
    }, {
        instanceMethods: {
            sync: function (xeroConfig) {
                var _this = this;
                var xero = new Xero(xeroConfig);
                var contacts = xero.contacts;
                return new Promise(function (resolve, reject) {
                    contacts.findById(_this.get('ContactID'))
                        .then(function (contact) {
                        if (contact) {
                            resolve(_this.update({ profile: contact, lastSync: Date.now(), status: 'valid' }));
                        }
                        else {
                            resolve(_this.update({ status: 'invalid' }));
                        }
                    })
                        .catch(function (err) {
                        reject(err);
                    });
                });
            },
            invoice: function (DueDate, LineItems) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    if (!_this.valid)
                        return reject(new Error('Customer Not Valid'));
                    _this.getXeroOrganisation()
                        .then(function (instance) {
                        var xero = new Xero(instance.xeroConfiguration);
                        var invoices = xero.invoices;
                        var inv = {
                            DueDate: DueDate,
                            Type: 'ACCREC',
                            Status: 'DRAFT',
                            Contact: {
                                ContactID: _this.ContactID
                            },
                            LineItems: LineItems
                        };
                        invoices.create(inv)
                            .then(function (invoice) {
                            resolve(invoice);
                        })
                            .catch(reject);
                    });
                });
            }
        },
        hooks: {
            beforeCreate: function (instance, options, next) {
                Promise.all([
                    instance.getXeroOrganisation({ transaction: options.t }),
                    instance.getUser({ transaction: options.t })
                ])
                    .then(function (values) {
                    if (!values[0]) {
                        next();
                    }
                    else {
                        if (values[1] && !instance.profile && !instance.ContactID) {
                            values[0].createCustomer({
                                Name: values[1].name,
                                EmailAddress: values[1].email,
                                Phones: {
                                    Phone: {
                                        PhoneNumber: values[1].phone,
                                        PhoneType: 'MOBILE'
                                    }
                                }
                            })
                                .then(function (xeroProfile) {
                                if (xeroProfile) {
                                    instance.ContactID = xeroProfile.ContactID;
                                    instance.profile = xeroProfile;
                                    options.sync = true;
                                    next();
                                }
                                else
                                    next('Xero Contact Cannot Be Created');
                            });
                        }
                        else {
                            next();
                        }
                    }
                });
            },
            afterCreate: function (instance, options, next) {
                if (options.sync)
                    return next();
                if (instance.ContactID) {
                    instance.getXeroOrganisation({ transaction: options.t })
                        .then(function (xeroOrg) {
                        xeroOrg.findById(instance.ContactID)
                            .then(function (xeroProfile) {
                            if (xeroProfile) {
                                instance.update({
                                    profile: xeroProfile
                                })
                                    .then(function (updated) {
                                    instance = updated;
                                    next();
                                });
                            }
                            else {
                                next('Xero Contact Does Not Exist');
                            }
                        })
                            .catch(next);
                    });
                }
                else {
                    next('ContactID must be set to create Xero Customer');
                }
            }
        }
    });
    return XeroCustomers;
};

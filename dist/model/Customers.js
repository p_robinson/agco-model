module.exports = function (sequelize, DataTypes) {
    var Customers = sequelize.define('Customers', {
        UserId: {
            type: DataTypes.UUID,
            unique: 'Customers',
            primaryKey: true,
            references: {
                model: 'Users',
                key: 'id'
            }
        },
        OrganisationId: {
            type: DataTypes.UUID,
            unique: 'Customers',
            primaryKey: true,
            references: {
                model: 'Organisations',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });
    return Customers;
};

var path = require('path');
module.exports = function (sequelize, Sequelize) {
    var AuthorisedUsers = sequelize.import(path.join(__dirname, 'AuthorisedUsers.js'));
    var Customers = sequelize.import(path.join(__dirname, 'Customers.js'));
    var LocalLogin = sequelize.import(path.join(__dirname, 'LocalLogin.js'));
    var Organisations = sequelize.import(path.join(__dirname, 'Organisations.js'));
    var Systems = sequelize.import(path.join(__dirname, 'Systems.js'));
    var Users = sequelize.import(path.join(__dirname, 'Users.js'));
    var XeroCustomers = sequelize.import(path.join(__dirname, 'XeroCustomers.js'));
    var XeroOrganisations = sequelize.import(path.join(__dirname, 'XeroOrganisations.js'));
    var Services = sequelize.import(path.join(__dirname, 'Services.js'));
    var Notes = sequelize.import(path.join(__dirname, 'Notes.js'));
    var Locations = sequelize.import(path.join(__dirname, 'Locations.js'));
    var Commissions = sequelize.import(path.join(__dirname, 'Commissions.js'));
    var Councils = sequelize.import(path.join(__dirname, 'Councils.js'));
    var SystemOwners = sequelize.import(path.join(__dirname, 'SystemOwners.js'));
    var Parts = sequelize.import(path.join(__dirname, 'Parts.js'));
    Parts.belongsTo(Organisations);
    Organisations.hasMany(Parts);
    Parts.belongsTo(Systems);
    Systems.hasMany(Parts);
    Parts.hasMany(Notes, {
        constraints: false,
        scope: {
            type: 'PARTS'
        },
        foreignKey: 'tableId'
    });
    Notes.belongsTo(Parts, {
        constraints: false,
        scope: {
            type: 'PARTS'
        },
        foreignKey: 'tableId'
    });
    Notes.belongsTo(Parts, {
        constraints: false,
        scope: {
            type: 'COMMISSIONS'
        },
        foreignKey: 'tableId'
    });
    Commissions.hasMany(Notes, {
        constraints: false,
        scope: {
            type: 'COMMISSIONS'
        },
        foreignKey: 'tableId'
    });
    Locations.belongsTo(Organisations);
    Organisations.hasMany(Locations);
    Systems.hasOne(Locations);
    Locations.belongsTo(Systems);
    Organisations.hasMany(Commissions);
    Commissions.belongsTo(Organisations);
    Systems.hasOne(Commissions);
    Commissions.belongsTo(Systems);
    Users.hasMany(Commissions, {
        foreignKey: 'AuthorisedUserId'
    });
    Commissions.belongsTo(Users, {
        as: 'AuthorisedUser',
        foreignKey: 'AuthorisedUserId'
    });
    Notes.belongsTo(Systems, {
        constraints: false,
        scope: {
            type: 'SYSTEMS'
        },
        foreignKey: 'tableId'
    });
    Systems.hasMany(Notes, {
        constraints: false,
        scope: {
            type: 'SYSTEMS'
        },
        foreignKey: 'tableId'
    });
    Notes.belongsTo(Services, {
        constraints: false,
        foreignKey: 'tableId'
    });
    Services.hasMany(Notes, {
        constraints: false,
        scope: {
            type: 'SERVICES'
        },
        foreignKey: 'tableId'
    });
    Notes.belongsTo(Organisations);
    Organisations.hasMany(Notes);
    Notes.belongsTo(Users, {
        foreignKey: 'UserId',
        as: 'AuthorisedUser',
        constraints: false
    });
    Users.hasMany(Notes, {
        foreignKey: 'UserId',
        scope: {
            user: 'AUTHORISED'
        },
        constraints: false,
        as: 'AuthorisedUserNotes'
    });
    Notes.belongsTo(Users, {
        foreignKey: 'UserId',
        as: 'Owner',
        constraints: false
    });
    Users.hasMany(Notes, {
        foreignKey: 'UserId',
        scope: {
            user: 'OWNER'
        },
        constraints: false,
        as: 'OwnerNotes'
    });
    Users.hasMany(Notes, {
        as: 'CustomerNotes',
        constraints: false,
        scope: {
            type: 'CUSTOMERS'
        },
        foreignKey: 'tableId'
    });
    Notes.belongsTo(Users, {
        as: 'Customer',
        foreignKey: 'tableId',
        constraints: false
    });
    Organisations.belongsTo(Users, {
        as: 'Owner',
        foreignKey: 'UserId'
    });
    Organisations.belongsToMany(Users, {
        through: AuthorisedUsers,
        as: 'AuthdUsers',
        onDelete: 'CASCADE'
    });
    Organisations.belongsToMany(Users, {
        as: 'Customers',
        through: Customers,
        onDelete: 'CASCADE'
    });
    Users.hasMany(XeroCustomers, {
        as: 'XeroProfiles'
    });
    Organisations.hasMany(Systems);
    Organisations.hasMany(Services);
    Services.belongsTo(Systems);
    Services.belongsTo(Organisations);
    Services.belongsTo(Users, {
        foreignKey: 'ServiceAgentId',
        as: 'ServiceAgent'
    });
    Systems.hasMany(Services);
    Systems.belongsTo(Organisations, {
        as: 'Organisation'
    });
    Systems.belongsToMany(Users, {
        through: SystemOwners,
        as: 'Owners'
    });
    Users.belongsToMany(Systems, {
        through: SystemOwners,
        as: 'Systems'
    });
    Users.hasMany(Services, {
        foreignKey: 'ServiceAgentId',
    });
    Users.hasMany(Organisations, {
        as: 'OwnedOrganisations'
    });
    Users.hasOne(LocalLogin);
    Users.belongsToMany(Organisations, {
        through: AuthorisedUsers,
        as: 'AuthorisedOrganisations',
        onDelete: 'CASCADE'
    });
    Users.belongsToMany(Organisations, {
        through: Customers,
        as: 'CustomerAccounts',
        onDelete: 'CASCADE'
    });
    Organisations.hasOne(XeroOrganisations);
    XeroOrganisations.belongsTo(Organisations, {
        foreignKey: 'OrganisationId',
        targetKey: 'id'
    });
    Customers.hasMany(XeroCustomers, {
        as: 'XeroProfiles',
        foreignKey: 'UserId'
    });
    Customers.hasMany(Systems, {
        as: 'CurrentSystems',
        foreignKey: 'CurrentOwnerId'
    });
    XeroCustomers.belongsTo(XeroOrganisations, {
        foreignKey: 'XeroProfileId',
        targetKey: 'OrganisationId',
    });
    XeroOrganisations.hasMany(XeroCustomers, {
        foreignKey: 'XeroProfileId',
    });
    XeroCustomers.belongsTo(Users);
    Councils.hasMany(Locations);
    Locations.belongsTo(Councils);
    Users.hasMany(Systems, {
        as: 'CurrentSystems',
        foreignKey: 'CurrentOwnerId'
    });
    Systems.belongsTo(Users, {
        as: 'CurrentOwner',
        foreignKey: 'CurrentOwnerId'
    });
    return { sequelize: sequelize, Sequelize: Sequelize, Parts: Parts, Councils: Councils, AuthorisedUsers: AuthorisedUsers, Commissions: Commissions, Customers: Customers, LocalLogin: LocalLogin, Locations: Locations, Notes: Notes, Organisations: Organisations, Services: Services, Systems: Systems, Users: Users, XeroCustomers: XeroCustomers, XeroOrganisations: XeroOrganisations };
};

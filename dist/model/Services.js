module.exports = function (sequelize, DataTypes) {
    var Services = sequelize.define('Services', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        // Legacy
        'serviceId': {
            type: DataTypes.VIRTUAL,
            get: function () {
                return this.getDataValue('id');
            }
        },
        serviceDate: DataTypes.DATE,
        serviceData: {
            type: DataTypes.BLOB,
            get: function () {
                try {
                    var profile = JSON.parse(this.getDataValue('serviceData'));
                }
                catch (err) {
                    return;
                }
                return profile;
            },
            set: function (val) {
                this.setDataValue('serviceData', JSON.stringify(val));
            }
        },
        invoice: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        InvoiceID: DataTypes.UUID
    });
    Services.afterCreate('invoice', function (instance, options, next) {
        instance.reload({
            include: [{
                    model: sequelize.models.Systems,
                    include: [{
                            model: sequelize.models.Users,
                            as: 'CurrentOwner',
                            include: [{
                                    model: sequelize.models.XeroCustomers,
                                    as: 'XeroProfiles',
                                    scope: {}
                                }]
                        }]
                }, {
                    model: sequelize.models.Organisations,
                }]
        }).then(function (service) {
            if (service && service.get('System') && service.get('System').get('CurrentOwner') && service.get('System').get('CurrentOwner').get('XeroProfiles')) {
                var customer = service.get('System').get('CurrentOwner').get('XeroProfiles')[0];
                if (customer) {
                    var items = [];
                    items.push({
                        "ItemCode": "003",
                        "Description": "Servicing",
                        "UnitAmount": "75.00",
                        "LineAmount": "75.00",
                        "AccountCode": "191",
                        "Quantity": "1.0000"
                    });
                    if (service.serviceData && service.serviceData.chlorTabs > 0) {
                        items.push({
                            "ItemCode": "002",
                            "Description": "Chlorine",
                            "UnitAmount": "11.82",
                            "LineAmount": "11.82",
                            "AccountCode": "23003",
                            "Quantity": "1.0000"
                        });
                    }
                    customer.invoice(new Date(new Date().setDate(new Date().getDate() + 7)), items)
                        .then(function (invoice) {
                        if (invoice && invoice.Invoice.InvoiceID) {
                            service.update({ invoice: true, InvoiceID: invoice.Invoice.InvoiceID })
                                .then(function (service) {
                                instance = service;
                                next();
                            })
                                .catch(function (instance) {
                                next();
                            });
                        }
                        else
                            next();
                    })
                        .catch(function (err) {
                        console.log(err);
                        next();
                    });
                }
                else
                    next();
            }
            else
                next();
        });
    });
    Services.afterCreate('updateSystem', function (instance, options, next) {
        if (instance && instance.serviceDate instanceof Date) {
            var nextService = new Date(instance.serviceDate);
            instance.getSystem()
                .then(function (system) {
                if (system) {
                    system.update({
                        lastService: instance.serviceDate,
                        nextService: nextService.setDate(nextService.getDate() + system.serviceInterval)
                    })
                        .then(function (system) {
                        next();
                    })
                        .catch(next);
                }
                else
                    next();
            }).catch(next);
        }
        else
            next();
    });
    return Services;
};

module.exports = function (sequelize, DataTypes) {
    var Locations = sequelize.define('Locations', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        address: DataTypes.STRING,
        city: DataTypes.STRING,
        state: DataTypes.STRING,
        postcode: DataTypes.STRING,
    });
    return Locations;
};

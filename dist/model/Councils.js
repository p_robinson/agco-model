module.exports = function (sequelize, DataTypes) {
    var Councils = sequelize.define('Councils', {
        name: {
            type: DataTypes.STRING
        },
        suburb: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.STRING
        },
        fax: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        }
    });
    return Councils;
};

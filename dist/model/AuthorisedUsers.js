module.exports = function (sequelize, DataTypes) {
    var AuthorisedUsers = sequelize.define('AuthorisedUsers', {
        status: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });
    return AuthorisedUsers;
};

module.exports = function (sequelize, DataTypes) {
    var Users = sequelize.define('Users', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        'customer_idcustomer': {
            //Legacy
            type: DataTypes.VIRTUAL,
            get: function () {
                return this.getDataValue('name');
            }
        },
        'idcustomer': {
            //Legacy
            type: DataTypes.VIRTUAL,
            get: function () {
                return this.getDataValue('name');
            }
        },
        name: DataTypes.STRING,
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        email: DataTypes.STRING,
        mobile: DataTypes.STRING
    });
    return Users;
};
;
;

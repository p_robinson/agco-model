var path = require('path');
var Sequelize = require('sequelize');
exports.Sequelize = Sequelize;
var basename = path.basename(module.filename);
var env = process.env.NODE_ENV || 'development';
var sequelize = new Sequelize('test', 'root', null, {
    host: '127.0.0.1',
    dialect: 'mysql',
    logging: false
});
exports.sequelize = sequelize;
var db = {};
var AuthorisedUsers = sequelize.import(path.join(__dirname, 'AuthorisedUsers.js'));
exports.AuthorisedUsers = AuthorisedUsers;
var Customers = sequelize.import(path.join(__dirname, 'Customers.js'));
exports.Customers = Customers;
var LocalLogin = sequelize.import(path.join(__dirname, 'LocalLogin.js'));
exports.LocalLogin = LocalLogin;
var Organisations = sequelize.import(path.join(__dirname, 'Organisations.js'));
exports.Organisations = Organisations;
var Systems = sequelize.import(path.join(__dirname, 'Systems.js'));
exports.Systems = Systems;
var Users = sequelize.import(path.join(__dirname, 'Users.js'));
exports.Users = Users;
var XeroCustomers = sequelize.import(path.join(__dirname, 'XeroCustomers.js'));
exports.XeroCustomers = XeroCustomers;
var XeroOrganisations = sequelize.import(path.join(__dirname, 'XeroOrganisations.js'));
exports.XeroOrganisations = XeroOrganisations;
var Services = sequelize.import(path.join(__dirname, 'Services.js'));
exports.Services = Services;
var Notes = sequelize.import(path.join(__dirname, 'Notes.js'));

var Sequelize = require('sequelize');
var Xero = require('xero-extension');
module.exports = function (sequelize, DataTypes) {
    var XeroOrganisations = sequelize.define('XeroOrganisations', {
        OrganisationId: {
            type: Sequelize.UUID,
            references: {
                model: 'Organisations',
                key: 'id'
            }
        },
        key: {
            type: DataTypes.STRING,
            allowNull: false
        },
        secret: {
            type: DataTypes.STRING,
            allowNull: false
        },
        rsa_key: {
            type: DataTypes.BLOB,
            allowNull: false
        },
        ShortCode: {
            type: DataTypes.STRING
        },
        lastSync: {
            type: DataTypes.DATE
        },
        status: {
            type: DataTypes.ENUM(['valid', 'invalid',]),
            defaultValue: 'invalid'
        },
        xeroConfiguration: {
            type: DataTypes.VIRTUAL,
            get: function () {
                return { key: this.key, secret: this.secret, rsa_key: this.rsa_key };
            }
        },
        profile: {
            type: Sequelize.BLOB,
            get: function () {
                return JSON.parse(this.getDataValue('profile'));
            },
            set: function (val) {
                this.setDataValue('profile', JSON.stringify(val));
            }
        }
    }, { instanceMethods: {
            createCustomer: function (options) {
                if (options === void 0) { options = {}; }
                var xero = new Xero(this.xeroConfiguration);
                var contacts = xero.contacts;
                return new Promise(function (resolve, reject) {
                    contacts.createOrUpdate(options)
                        .then(function (contact) {
                        resolve(contact);
                    })
                        .catch(function (err) {
                        reject(err);
                    });
                });
            },
            findById: function (id) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    if (!id)
                        return reject(new Error('Xero ContactID must be set'));
                    var xero = new Xero(_this.xeroConfiguration);
                    var contacts = xero.contacts;
                    contacts.findById(id)
                        .then(function (contact) {
                        resolve(contact);
                    })
                        .catch(function (err) {
                        reject(err);
                    });
                });
            },
            sync: function (options) {
                var _this = this;
                if (options === void 0) { options = {}; }
                var xero = new Xero(this.xeroConfiguration);
                var organisation = xero.organisation;
                return new Promise(function (resolve, reject) {
                    organisation.findAll(false)
                        .then(function (organisation) {
                        resolve(_this.update({ profile: organisation, lastSync: Date.now(), status: 'valid', ShortCode: organisation.Organisations.Organisation.ShortCode }, { transaction: options.t }));
                    })
                        .catch(function (err) {
                        reject(err);
                    });
                });
            }
        }, hooks: {
            beforeCreate: function (xeroOrg, options, next) {
                var xero = new Xero(xeroOrg);
                var Organisation = xero.organisation;
                Organisation.findAll(false)
                    .then(function (organisation) {
                    xeroOrg.profile = organisation;
                    xeroOrg.lastSync = Date.now();
                    xeroOrg.status = 'valid';
                    xeroOrg.ShortCode = organisation.Organisations.Organisation.ShortCode;
                    next();
                })
                    .catch(next);
            }
        } });
    return XeroOrganisations;
};

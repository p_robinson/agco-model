module.exports = function (sequelize, DataTypes) {
    var Organisations = sequelize.define('Organisations', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        }
    });
    return Organisations;
};
;

module.exports = function (sequelize, DataTypes) {
    var Systems = sequelize.define('Systems', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        'sold': {
            //legacy
            type: DataTypes.VIRTUAL,
            get: function () {
                return 0;
            }
        },
        'ozzikleenSystems_serial': {
            //Legacy
            type: DataTypes.VIRTUAL,
            get: function () {
                return this.getDataValue('serial');
            }
        },
        serial: DataTypes.STRING,
        selling: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        serviceInterval: {
            type: DataTypes.INTEGER,
            defaultValue: 90
        },
        commission: DataTypes.DATE,
        lastService: DataTypes.DATE,
        nextService: DataTypes.DATE,
        service: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        status: DataTypes.ENUM(['ORDERED', 'DELIVERED', 'AWAITING_COMMISSION', 'COMMISSIONED']),
        type: DataTypes.ENUM(['OZZIKLEEN', 'FUJICLEAN', 'KRYSTALKLEAR']),
        days: {
            type: DataTypes.VIRTUAL,
            get: function () {
                if (this.lastService instanceof Date)
                    return Math.floor((new Date() - this.lastService) / (1000 * 60 * 60 * 24));
                return undefined;
            }
        }
    });
    return Systems;
};

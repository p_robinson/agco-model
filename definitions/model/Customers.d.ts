import { Instance, Model, HasOneGetAssociationMixin, HasOneSetAssociationMixin, HasOneCreateAssociationMixin } from 'sequelize';
import { XeroCustomerInstance, XeroCustomerAttributes } from './XeroCustomers';
export interface CustomerAttributes {
}
export interface CustomerInstance extends Instance<CustomerInstance, CustomerAttributes> {
    getXeroProfile: HasOneGetAssociationMixin<XeroCustomerInstance>;
    setXeroProfile: HasOneSetAssociationMixin<XeroCustomerInstance, number>;
    createXeroProfile: HasOneCreateAssociationMixin<XeroCustomerAttributes>;
}
export interface CustomerModel extends Model<CustomerInstance, CustomerAttributes> {
}

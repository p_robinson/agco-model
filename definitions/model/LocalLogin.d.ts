import { attributes, instanceMethods, classMethods } from 'prUser';
import { BelongsToGetAssociationMixin, BelongsToCreateAssociationMixin, BelongsToSetAssociationMixin } from 'sequelize';
import { UserAttributes, UserInstance } from './Users';
import * as Sequelize from 'sequelize';
export interface LocalLoginAttributes extends attributes.IAttributes {
    user?: UserAttributes;
}
export interface LocalLoginInstance extends Sequelize.Instance<LocalLoginInstance, LocalLoginAttributes>, instanceMethods.IInstanceMethods<LocalLoginInstance> {
    getUser: BelongsToGetAssociationMixin<UserInstance>;
    createUser: BelongsToCreateAssociationMixin<UserInstance>;
    setUser: BelongsToSetAssociationMixin<UserInstance, number>;
}
export interface LocalLoginModel extends Sequelize.Model<LocalLoginInstance, LocalLoginAttributes>, classMethods.IClassMethods {
}

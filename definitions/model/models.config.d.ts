import * as Sequelize from 'sequelize';
import { UserModel } from './Users';
import { OrganisationModel } from './Organisations';
import { XeroOrganisationModel } from './XeroOrganisations';
import { XeroCustomerModel } from './XeroCustomers';
import { LocalLoginModel } from './LocalLogin';
import { CustomerModel } from './Customers';
declare var sequelize: Sequelize.Sequelize;
export interface IModels {
    Users?: UserModel;
    Organisations?: OrganisationModel;
    XeroOrganisations?: XeroOrganisationModel;
    XeroCustomers?: XeroCustomerModel;
    LocalLogin?: LocalLoginModel;
    Customers?: CustomerModel;
    AuthorisedUsers?: any;
    Systems?: any;
}
export interface IDatabase extends IModels {
    Sequelize?: Sequelize.SequelizeStatic;
    sequelize?: Sequelize.Sequelize;
}
declare var AuthorisedUsers: any;
declare var Customers: CustomerModel;
declare var LocalLogin: LocalLoginModel;
declare var Organisations: OrganisationModel;
declare var Systems: any;
declare var Users: UserModel;
declare var XeroCustomers: any;
declare var XeroOrganisations: any;
declare var Services: any;
export { sequelize, Sequelize, AuthorisedUsers, Customers, LocalLogin, Organisations, Services, Systems, Users, XeroCustomers, XeroOrganisations };

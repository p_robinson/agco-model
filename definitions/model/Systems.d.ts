import { Instance, Model, BelongsToGetAssociationMixin, BelongsToSetAssociationMixin, BelongsToCreateAssociationMixin, BelongsToManyGetAssociationsMixin, BelongsToManySetAssociationsMixin, BelongsToManyAddAssociationsMixin, BelongsToManyAddAssociationMixin, BelongsToManyCreateAssociationMixin, BelongsToManyRemoveAssociationMixin, BelongsToManyRemoveAssociationsMixin, BelongsToManyHasAssociationMixin, BelongsToManyHasAssociationsMixin, BelongsToManyCountAssociationsMixin } from 'sequelize';
import { OrganisationInstance, OrganisationAttributes } from './Organisations';
import { CustomerInstance, CustomerAttributes } from './Customers';
export interface SystemAttributes {
    lastService?: string;
    nextService?: string;
    status?: string;
    type?: string;
}
export interface SystemInstance extends Instance<SystemInstance, SystemAttributes> {
    getOrganisation: BelongsToGetAssociationMixin<OrganisationInstance>;
    setOrganisation: BelongsToSetAssociationMixin<OrganisationInstance, number>;
    createOrganisation: BelongsToCreateAssociationMixin<OrganisationAttributes>;
    getCustomers: BelongsToManyGetAssociationsMixin<CustomerInstance>;
    setCustomers: BelongsToManySetAssociationsMixin<CustomerInstance, number, CustomerAttributes>;
    addCustomers: BelongsToManyAddAssociationsMixin<CustomerInstance, number, CustomerAttributes>;
    addCustomer: BelongsToManyAddAssociationMixin<CustomerInstance, number, CustomerAttributes>;
    createCustomer: BelongsToManyCreateAssociationMixin<CustomerAttributes, CustomerAttributes>;
    removeCustomer: BelongsToManyRemoveAssociationMixin<CustomerInstance, number>;
    removeCustomers: BelongsToManyRemoveAssociationsMixin<CustomerInstance, number>;
    hasCustomer: BelongsToManyHasAssociationMixin<CustomerInstance, number>;
    hasCustomers: BelongsToManyHasAssociationsMixin<CustomerInstance, number>;
    countCustomers: BelongsToManyCountAssociationsMixin;
}
export interface SystemModel extends Model<SystemInstance, SystemAttributes> {
}

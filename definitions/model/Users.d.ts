import { Instance, Model, HasManyCountAssociationsMixin, HasManyHasAssociationsMixin, HasManyHasAssociationMixin, HasManyRemoveAssociationsMixin, HasManyRemoveAssociationMixin, HasManyCreateAssociationMixin, HasManyAddAssociationMixin, HasManyGetAssociationsMixin, HasManySetAssociationsMixin, HasManyAddAssociationsMixin, BelongsToManyGetAssociationsMixin, BelongsToManySetAssociationsMixin, BelongsToManyAddAssociationsMixin, BelongsToManyAddAssociationMixin, BelongsToManyCreateAssociationMixin, BelongsToManyRemoveAssociationMixin, BelongsToManyRemoveAssociationsMixin, BelongsToManyHasAssociationMixin, BelongsToManyHasAssociationsMixin, BelongsToManyCountAssociationsMixin, HasOneGetAssociationMixin, HasOneCreateAssociationMixin, HasOneSetAssociationMixin } from 'sequelize';
import { LocalLoginInstance } from './LocalLogin';
import { OrganisationInstance, OrganisationAttributes } from './Organisations';
import { LocalLoginAttributes } from './LocalLogin';
import { AuthorisedUserAttributes } from './AuthorisedUsers';
import { CustomerAttributes } from './Customers';
export interface UserAttributes {
    Organisations?: OrganisationAttributes[];
    LocalLogin?: LocalLoginAttributes;
    OwnedOrganisations?: OrganisationAttributes[];
    AssociatedOrganisations?: OrganisationAttributes[];
}
export interface UserInstance extends Instance<UserInstance, UserAttributes> {
    getOwnedOrganisations: HasManyGetAssociationsMixin<OrganisationInstance>;
    setOwnedOrganisations: HasManySetAssociationsMixin<OrganisationInstance, number>;
    addOwnedOrganisations: HasManyAddAssociationsMixin<OrganisationInstance, number>;
    addOwnedOrganisation: HasManyAddAssociationMixin<OrganisationInstance, number>;
    createOwnedOrganisation: HasManyCreateAssociationMixin<OrganisationAttributes>;
    removeOwnedOrganisation: HasManyRemoveAssociationMixin<OrganisationInstance, number>;
    removeOwnedOrganisations: HasManyRemoveAssociationsMixin<OrganisationInstance, number>;
    hasOwnedOrganisation: HasManyHasAssociationMixin<OrganisationInstance, number>;
    hasOwnedOrganisations: HasManyHasAssociationsMixin<OrganisationInstance, number>;
    countOwnedOrganisations: HasManyCountAssociationsMixin;
    getLocalLogin: HasOneGetAssociationMixin<LocalLoginInstance>;
    createLocalLogin: HasOneCreateAssociationMixin<LocalLoginInstance>;
    setLocalLogin: HasOneSetAssociationMixin<LocalLoginInstance, number>;
    getAuthorisedOrganisations: BelongsToManyGetAssociationsMixin<OrganisationInstance>;
    setAuthorisedOrganisations: BelongsToManySetAssociationsMixin<OrganisationInstance, number, AuthorisedUserAttributes>;
    addAuthorisedOrganisations: BelongsToManyAddAssociationsMixin<OrganisationInstance, number, AuthorisedUserAttributes>;
    addAuthorisedOrganisation: BelongsToManyAddAssociationMixin<OrganisationInstance, number, AuthorisedUserAttributes>;
    createAuthorisedOrganisation: BelongsToManyCreateAssociationMixin<OrganisationAttributes, AuthorisedUserAttributes>;
    removeAuthorisedOrganisation: BelongsToManyRemoveAssociationMixin<OrganisationInstance, number>;
    removeAuthorisedOrganisations: BelongsToManyRemoveAssociationsMixin<OrganisationInstance, number>;
    hasAuthorisedOrganisation: BelongsToManyHasAssociationMixin<OrganisationInstance, number>;
    hasAuthorisedOrganisations: BelongsToManyHasAssociationsMixin<OrganisationInstance, number>;
    countAuthorisedOrganisations: BelongsToManyCountAssociationsMixin;
    getCustomerAccounts: BelongsToManyGetAssociationsMixin<OrganisationInstance>;
    setCustomerAccounts: BelongsToManySetAssociationsMixin<OrganisationInstance, number, CustomerAttributes>;
    addCustomerAccounts: BelongsToManyAddAssociationsMixin<OrganisationInstance, number, CustomerAttributes>;
    addCustomerAccount: BelongsToManyAddAssociationMixin<OrganisationInstance, number, CustomerAttributes>;
    createCustomerAccount: BelongsToManyCreateAssociationMixin<OrganisationAttributes, CustomerAttributes>;
    removeCustomerAccount: BelongsToManyRemoveAssociationMixin<OrganisationInstance, number>;
    removeCustomerAccounts: BelongsToManyRemoveAssociationsMixin<OrganisationInstance, number>;
    hasCustomerAccount: BelongsToManyHasAssociationMixin<OrganisationInstance, number>;
    hasCustomerAccounts: BelongsToManyHasAssociationsMixin<OrganisationInstance, number>;
    countCustomerAccounts: BelongsToManyCountAssociationsMixin;
}
export interface UserModel extends Model<UserInstance, UserAttributes> {
}

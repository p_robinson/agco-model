export declare var XeroOrganisation: {
    key: string;
    rsa_key: string;
    secret: string;
};

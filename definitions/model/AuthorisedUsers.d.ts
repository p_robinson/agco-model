import { Instance, Model } from 'sequelize';
export interface AuthorisedUserAttributes {
}
export interface AuthorisedUserInstance extends Instance<AuthorisedUserInstance, AuthorisedUserAttributes> {
}
export interface AuthorisedUserModel extends Model<AuthorisedUserInstance, AuthorisedUserAttributes> {
}

import { Instance, Model, BelongsToGetAssociationMixin, BelongsToSetAssociationMixin, BelongsToCreateAssociationMixin, BelongsToManyGetAssociationsMixin, BelongsToManySetAssociationsMixin, BelongsToManyAddAssociationsMixin, BelongsToManyAddAssociationMixin, BelongsToManyCreateAssociationMixin, BelongsToManyRemoveAssociationMixin, BelongsToManyRemoveAssociationsMixin, BelongsToManyHasAssociationMixin, BelongsToManyHasAssociationsMixin, BelongsToManyCountAssociationsMixin, HasOneGetAssociationMixin, HasOneCreateAssociationMixin, HasOneSetAssociationMixin } from 'sequelize';
import { UserInstance, UserAttributes } from './Users';
import { XeroOrganisationInstance, XeroOrganisationAttributes } from './XeroOrganisations';
import { CustomerAttributes } from './Customers';
import { AuthorisedUserAttributes } from './AuthorisedUsers';
export interface OrganisationAttributes {
}
export interface OrganisationInstance extends Instance<OrganisationInstance, OrganisationAttributes> {
    getOwner: BelongsToGetAssociationMixin<UserInstance>;
    setOwner: BelongsToSetAssociationMixin<UserInstance, number>;
    createOwner: BelongsToCreateAssociationMixin<UserAttributes>;
    getXeroOrganisation: HasOneGetAssociationMixin<XeroOrganisationInstance>;
    setXeroOrganisation: HasOneSetAssociationMixin<XeroOrganisationInstance, number>;
    createXeroOrganisation: HasOneCreateAssociationMixin<XeroOrganisationAttributes>;
    getCustomers: BelongsToManyGetAssociationsMixin<UserInstance>;
    setCustomers: BelongsToManySetAssociationsMixin<UserInstance, number, CustomerAttributes>;
    addCustomers: BelongsToManyAddAssociationsMixin<UserInstance, number, CustomerAttributes>;
    addCustomer: BelongsToManyAddAssociationMixin<UserInstance, number, CustomerAttributes>;
    createCustomer: BelongsToManyCreateAssociationMixin<UserAttributes, CustomerAttributes>;
    removeCustomer: BelongsToManyRemoveAssociationMixin<UserInstance, number>;
    removeCustomers: BelongsToManyRemoveAssociationsMixin<UserInstance, number>;
    hasCustomer: BelongsToManyHasAssociationMixin<UserInstance, number>;
    hasCustomers: BelongsToManyHasAssociationsMixin<UserInstance, number>;
    countCustomers: BelongsToManyCountAssociationsMixin;
    getAuthorisedUsers: BelongsToManyGetAssociationsMixin<UserInstance>;
    setAuthorisedUsers: BelongsToManySetAssociationsMixin<UserInstance, number, AuthorisedUserAttributes>;
    addAuthorisedUsers: BelongsToManyAddAssociationsMixin<UserInstance, number, AuthorisedUserAttributes>;
    addAuthorisedUser: BelongsToManyAddAssociationMixin<UserInstance, number, AuthorisedUserAttributes>;
    createAuthorisedUser: BelongsToManyCreateAssociationMixin<UserAttributes, AuthorisedUserAttributes>;
    removeAuthorisedUser: BelongsToManyRemoveAssociationMixin<UserInstance, number>;
    removeAuthorisedUsers: BelongsToManyRemoveAssociationsMixin<UserInstance, number>;
    hasAuthorisedUser: BelongsToManyHasAssociationMixin<UserInstance, number>;
    hasAuthorisedUsers: BelongsToManyHasAssociationsMixin<UserInstance, number>;
    countAuthorisedUsers: BelongsToManyCountAssociationsMixin;
}
export interface OrganisationModel extends Model<OrganisationInstance, OrganisationAttributes> {
}

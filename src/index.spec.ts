import * as chai from 'chai';
import * as fs from 'fs';
import { sequelize, Organisations, Users, XeroOrganisations } from './model';


describe('AgcoModel', () => {
  before(done => {

    sequelize.sync({force: true}).then(() => {
      done()
    })
  })

  it.skip('should also exist', function (done) {
    this.timeout(10000)
    XeroOrganisations.create({ key: 'LMKQROSLOKQAIHUNIIRQAHJQOPKZQE', rsa_key: fs.readFileSync('/users/home/privatekey.pem', 'UTF-8'), secret: 'KZB70N8FJCYAOQ9TK2JCVI7DH7RYI2' }).then(xeroOrg => {
      xeroOrg.createXeroCustomer({ ContactID: '565acaa9-e7f3-4fbf-80c3-16b081ddae10' }).then(() => {
        console.log('here too')
        done()
      }).catch(done)
    }).catch(done)
  })
})

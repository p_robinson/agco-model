import * as instanceMethods from './instanceMethods';
import * as classMethods from './ClassMethods';
import * as attributes from './Attributes';
import * as hooks from './Hooks';
import * as getterMethods from './GetterMethods';
import * as setterMethods from './SetterMethods';
import * as scopes from './Scopes';
import * as defaultScope from './DefaultScope';
export { instanceMethods, classMethods, attributes, hooks, getterMethods, setterMethods, scopes, defaultScope }



export var modelOptions = { instanceMethods, classMethods };

if (getterMethods) { modelOptions['getterMethods'] = getterMethods }
if (setterMethods) { modelOptions['setterMethods'] = setterMethods }
if (hooks) { modelOptions['hooks'] = hooks }

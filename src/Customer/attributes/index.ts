import * as Sequelize from 'sequelize';
export interface IAttributes {}
export var attributes: Sequelize.DefineAttributes = {
  name: {
    type: Sequelize.STRING
  },
  mobile: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: {msg: 'Not a Valid Email'}
    }
  }
}


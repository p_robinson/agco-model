import { instanceMethods, classMethods, attributes, hooks, getterMethods, setterMethods, defaultScope, scopes }  from '../';
import * as Sequelize from 'sequelize';
import * as chai from 'chai';
var s;
var Customer;
interface CustomerAttributes extends attributes.IAttributes { };
interface CustomerInstance extends Sequelize.Instance <CustomerInstance, CustomerAttributes >, instanceMethods.IInstanceMethods <CustomerInstance > {}
interface CustomerModel < TInstance, TAttributes > extends Sequelize.Model < TInstance, TAttributes >, classMethods.IClassMethods <CustomerInstance> {};
describe('Customer getterMethods', () => {
  beforeEach(done => {
    s = new Sequelize('test', 'root', null, {
      host: '127.0.0.1',
      dialect: 'mysql',
      logging: false
    })
      Customer = <CustomerModel<CustomerInstance, CustomerAttributes>> s.define('Customer', attributes.attributes, { instanceMethods, classMethods, hooks, getterMethods, setterMethods, defaultScope, scopes })
      s.synch({force: true}).then(() => {
        done()
      })
  })
})

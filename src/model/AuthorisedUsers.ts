import { Sequelize, DataTypes, Instance, Model } from 'sequelize';

module.exports = function (sequelize: Sequelize, DataTypes: DataTypes) {
  var AuthorisedUsers = sequelize.define('AuthorisedUsers', {
    status: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
      }
    }
  })
  return AuthorisedUsers
}
export interface AuthorisedUserAttributes {}
export interface AuthorisedUserInstance extends Instance<AuthorisedUserInstance, AuthorisedUserAttributes> {}
export interface AuthorisedUserModel extends Model<AuthorisedUserInstance, AuthorisedUserAttributes> { }

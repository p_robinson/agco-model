import { Sequelize, DataTypes } from 'sequelize';
module.exports = function (sequelize: Sequelize, DataTypes: DataTypes) {
  var Parts = sequelize.define('Parts', {
    type: DataTypes.STRING,
    model: DataTypes.STRING,
    serial: DataTypes.STRING,
    installed: DataTypes.DATE,
    removed: DataTypes.DATE
  })
  return Parts
}

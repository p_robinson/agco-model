import { Sequelize, DataTypes } from 'sequelize';
module.exports = function (sequelize: Sequelize, DataTypes:DataTypes) {
  var Services = sequelize.define('Services', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    // Legacy
    'serviceId': {
      type: DataTypes.VIRTUAL,
      get: function () {
        return this.getDataValue('id')
      }
    },
    serviceDate: DataTypes.DATE,

    serviceData: {
      type: DataTypes.BLOB,
      get: function() {
        try {
          var profile = JSON.parse(this.getDataValue('serviceData'))
        }
        catch (err) {
          return;
        }
        return profile
        },
        set: function(val) {
          this.setDataValue('serviceData', JSON.stringify(val))
    }
    },
    invoice: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    InvoiceID: DataTypes.UUID
  })
  Services.afterCreate('invoice', function (instance, options, next) {
      instance.reload({
        include: [{
          model: sequelize.models.Systems,
          include: [{
            model: sequelize.models.Users,
            as: 'CurrentOwner',
            include: [{
              model: sequelize.models.XeroCustomers,
              as: 'XeroProfiles',
              scope: {

              }
            }]
          }]
        }, {
          model: sequelize.models.Organisations,

        }]
      }).then(service => {
      if (service && service.get('System') && service.get('System').get('CurrentOwner') && service.get('System').get('CurrentOwner').get('XeroProfiles')) {
          var customer = service.get('System').get('CurrentOwner').get('XeroProfiles')[0];
          if (customer) {
            var items = []
            items.push({
              "ItemCode": "003",
              "Description": "Servicing",
              "UnitAmount": "75.00",
              "LineAmount": "75.00",
              "AccountCode": "191",
              "Quantity": "1.0000"
            })

            if (service.serviceData.chlorTabs > 0) {
              items.push({
              "ItemCode": "002",
              "Description": "Chlorine",
              "UnitAmount": "13.00",
              "LineAmount": "13.00",
              "AccountCode": "23003",
              "Quantity": "1.0000"
            });
            }


            customer.invoice(new Date(new Date().setDate(new Date().getDate() + 7)), items)
            .then(invoice => {
              if (invoice && invoice.Invoice.InvoiceID) {
                service.update({ invoice: true, InvoiceID: invoice.Invoice.InvoiceID })
                .then(service => {
                  instance = service
                  next()
                })
                .catch(instance => {
                  next()
                })
              }
              else next()
            })
            .catch(err => {
              console.log(err)
              next()
            })
          }
          else next();
        }
      else next()
      })

  })
  Services.afterCreate('updateSystem', function (instance, options, next) {

    if (instance && instance.serviceDate instanceof Date) {
      var nextService = new Date(instance.serviceDate)
      instance.getSystem()
      .then(system => {
        if (system) {
        system.update({
          lastService: instance.serviceDate,
          nextService: nextService.setDate(nextService.getDate() + system.serviceInterval)
           })
        .then(system => {
          next()
        })
        .catch(next)
        }
      else next()
    }).catch(next)
    }
    else next()
  })

  return Services
}

import * as fs from 'fs';
import * as path from 'path';
import * as Sequelize from 'sequelize';
import { UserModel } from './Users';
import { OrganisationModel } from './Organisations';
import { XeroOrganisationModel} from './XeroOrganisations';
import { XeroCustomerModel } from './XeroCustomers';
import { LocalLoginModel } from './LocalLogin';
import { CustomerModel } from './Customers';
var basename = path.basename(module.filename);
var env = process.env.NODE_ENV || 'development';
var sequelize = new Sequelize('test', 'root', null, {
  host: '127.0.0.1',
  dialect: 'mysql',
  logging: false
});


export interface IModels {
  Users?: UserModel;
  Organisations?: OrganisationModel;
  XeroOrganisations?: XeroOrganisationModel;
  XeroCustomers?: XeroCustomerModel;
  LocalLogin?: LocalLoginModel;
  Customers?: CustomerModel;
  AuthorisedUsers?: any;
  Systems?: any;
}
export interface IDatabase extends IModels {
  Sequelize?: Sequelize.SequelizeStatic,
  sequelize?: Sequelize.Sequelize,
}
var db = {};

var AuthorisedUsers = <any>sequelize.import(path.join(__dirname, 'AuthorisedUsers.js'))
var Customers = <CustomerModel>sequelize.import(path.join(__dirname, 'Customers.js'))
var LocalLogin = <LocalLoginModel>sequelize.import(path.join(__dirname, 'LocalLogin.js'))
var Organisations = <OrganisationModel>sequelize.import(path.join(__dirname, 'Organisations.js'))
var Systems = <any>sequelize.import(path.join(__dirname, 'Systems.js'))
var Users = <UserModel>sequelize.import(path.join(__dirname, 'Users.js'))
var XeroCustomers = <XeroCustomerModel>sequelize.import(path.join(__dirname, 'XeroCustomers.js'))
var XeroOrganisations = <XeroOrganisationModel>sequelize.import(path.join(__dirname, 'XeroOrganisations.js'))
var Services = <any>sequelize.import(path.join(__dirname, 'Services.js'))
var Notes = <any>sequelize.import(path.join(__dirname, 'Notes.js'))





export { sequelize, Sequelize, AuthorisedUsers, Customers, LocalLogin, Organisations, Services, Systems, Users, XeroCustomers, XeroOrganisations}

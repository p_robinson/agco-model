import * as Sequelize from 'sequelize';
import { IModels } from './';
var Xero = require('xero-extension');
module.exports = function (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
  var XeroCustomers = sequelize.define('XeroCustomers', {
    ContactID: {
      type: DataTypes.STRING,
    },
    XeroProfileId: {
      type: DataTypes.UUID,
      references: {
        model: 'XeroOrganisations',
        key: 'OrganisationId'
      },
      allowNull: false
    },
    profile: {
      type: DataTypes.BLOB,
      get: function() {
        try {
          var profile = JSON.parse(this.getDataValue('profile'))
        }
        catch (err) {
          return;
        }
        return profile
      },
      set: function(val) {
        this.setDataValue('profile', JSON.stringify(val))
      }
    },
    valid: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    lastSync: {
      type: DataTypes.DATE
    }
  }, {
    instanceMethods: {
      sync: function (xeroConfig: any){
  var xero = new Xero(xeroConfig)
  var contacts = xero.contacts
  return new Promise((resolve, reject) => {
    contacts.findById(this.get('ContactID'))
      .then(contact => {
        if (contact) {
          resolve(this.update({ profile: contact, lastSync: Date.now(), status: 'valid' }))
        }
        else {
          resolve(this.update({status: 'invalid'}))
        }
      })
      .catch(err => {
        reject(err)
      })
  })
},
    invoice: function (DueDate, LineItems) {
      return new Promise((resolve, reject) => {
        if (!this.valid) return reject(new Error('Customer Not Valid'))
        this.getXeroOrganisation()
          .then(instance => {
            var xero = new Xero(instance.xeroConfiguration)
            var invoices = xero.invoices

            var inv = {
              DueDate: DueDate,
              Type: 'ACCREC',
              Status: 'DRAFT',
              Contact: {
                ContactID: this.ContactID
              },
              LineItems: LineItems
            }

            invoices.create(inv)
              .then(invoice => {
                resolve(invoice)
              })
              .catch(reject)
          })
      })
    }


    },
    hooks: {
    beforeCreate: function (instance, options, next) {

Promise.all([
  instance.getXeroOrganisation({ transaction: options.t }),
  instance.getUser({transaction: options.t})
  ])
.then(values => {
  if (!values[0]) {
    next()
  }
  else {
    if (values[1] && !instance.profile && !instance.ContactID) {
    values[0].createCustomer({
            Name: values[1].name,
            EmailAddress: values[1].email,
            Phones: {
              Phone: {
                PhoneNumber: values[1].phone,
                PhoneType: 'MOBILE'
              }
            }
          })
    .then(xeroProfile => {
      if (xeroProfile) {
        instance.ContactID = xeroProfile.ContactID
        instance.profile = xeroProfile
        options.sync = true
        next()
      }
      else next('Xero Contact Cannot Be Created')
    })
    }
    else {
      next()
    }
  }
})
},
    afterCreate: function  (instance, options, next) {
      if (options.sync) return next();
      if (instance.ContactID) {
        instance.getXeroOrganisation({ transaction: options.t })
          .then(xeroOrg => {
            xeroOrg.findById(instance.ContactID)
              .then(xeroProfile => {
                if (xeroProfile) {
                  instance.update({
                    profile: xeroProfile
                  })
                    .then(updated => {
                      instance = updated;
                      next()
                    })
                }
                else {
                  next('Xero Contact Does Not Exist')
                }

              })
              .catch(next)
          })
      }
      else {
        next('ContactID must be set to create Xero Customer')
      }
    }
    }
  })

  return XeroCustomers
}

import * as chai from 'chai';
import { Locations, Systems, Organisations, sequelize } from '../model';
describe('Locations', () => {
  describe('create locations and nested associations', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
      .catch(done)
    })
    it('should create location with nested system', done => {
      Locations.create({
        System: {}
      }, {
        include: [{
          model: Systems
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('System')
        done()
      })
      .catch(done)
    })
    it('should create location with nested organisation', done => {
      Locations.create({
        Organisation: {
          name: 'Organisation'
        }
      }, {
        include: [{
          model: Organisations
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Organisation')
        done()
      })
      .catch(done)
    })
  })
  describe('location.BelongsToOrganisationMixin', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Locations.create()
        .then(location => {
          this.location = location
          done()
        })
      }).catch(done)
    })
    it('createOrganisation', done => {
      this.location.createOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getOrganisation', done => {
      this.location.getOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setOrganisation', done => {
      this.location.setOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('location.BelongsToSystemMixin', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Locations.create()
        .then(location => {
          this.location = location
          done()
        })
      }).catch(done)
    })
    it('createSystem', done => {
      this.location.createSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getSystem', done => {
      this.location.getSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setSystem', done => {
      this.location.setSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
})

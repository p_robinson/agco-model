import { Sequelize, DataTypes } from 'sequelize';
module.exports = function (sequelize: Sequelize, DataTypes: DataTypes) {
 var SystemOwners = sequelize.define('SystemOwners', {
    sold: DataTypes.BOOLEAN
  })
  return SystemOwners
}

import * as chai from 'chai';
import { Notes, Services, Users, Systems, Organisations, sequelize } from '../model'
describe('Notes', () => {
  describe('Notes and nested', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
      .catch(done)
    })
    it('should create a note with nested Customer', done => {
      Notes.create({
        type: 'CUSTOMERS',
        Customer: {}
      }, {
        include: [{
          model: Users,
          as: 'Customer'
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Customer')
        done()
      })
      .catch(done)
    })
    it('should create a note with nested Service', done => {
      Notes.create({
        type: 'SERVICES',
        Service : {}
      }, {
        include: [{
          model: Services
        }]
      })
      .then(instance => {
        chai.expect(instance).to.haveOwnProperty('Service')

        chai.expect(instance.type).to.equal('SERVICES')
        done()
      })
      .catch(done)
    })
    it('should create a note with a nested system', done => {
      Notes.create({
        type: 'SYSTEMS',
        System: {}
      }, {
        include: [{
          model: Systems
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('System')
        done()
      })
      .catch(done)
    })
    it('should create a note with nested owner', done => {
      Notes.create({
        Owner: {}
      }, {
        include: [{
          model: Users,
          as: 'Owner'
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Owner')
        done()
      })
      .catch(done)
    })
    it('should create a note with nested authorised User', done => {
    Notes.create({
      AuthorisedUser: {}
    }, {
        include: [{
          model: Users,
          as: 'AuthorisedUser'
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
          .haveOwnProperty('AuthorisedUser')
        done()
      })
      .catch(done)
    })
    it('should create a note with nested organisation', done => {
      Notes.create({
        Organisation: {
          name: 'Organisation'
        }
      }, {
        include: [{
          model: Organisations
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Organisation')
        done()
      })
      .catch(done)
    })
  })
  describe('notes.BelongsToSystemsMixin', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Notes.create()
        .then(note => {
          this.note = note
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createSystem', done => {
      this.note.createSystem()
      .then(system => {
        chai.expect(system).to.exist
        done()
      })
      .catch(done)
    })
    it('getSystem', done => {
      this.note.getSystem()
      .then(system => {

        chai.expect(system).to.exist
        done()
      })
      .catch(done)
    })
    it('setSystem', done => {
      this.note.setSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('notes.BelongsToServicesMixin', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Notes.create()
        .then(note => {
          this.note = note
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createService', done => {
      this.note.createService()
      .then(service => {
        chai.expect(service).to.exist
        done()
      })
      .catch(done)
    })
    it('getService', done => {
      this.note.getService()
      .then(service => {

        chai.expect(service).to.exist
        done()
      })
      .catch(done)
    })
    it('setService', done => {
      this.note.setService()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('notes.BelongsToCustomersMixin', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Notes.create()
        .then(note => {
          this.note = note
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createCustomer', done => {
      this.note.createCustomer()
      .then(customer => {
        chai.expect(customer).to.exist
        done()
      })
      .catch(done)
    })
    it('getCustomer', done => {
      this.note.getCustomer()
      .then(customer => {

        chai.expect(customer).to.exist
        done()
      })
      .catch(done)
    })
    it('setCustomer', done => {
      this.note.setCustomer()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('notes.BelongsToOrganisationsMixin', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Notes.create()
        .then(note => {
          this.note = note
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createOrganisation', done => {
      this.note.createOrganisation()
      .then(organisation => {
        chai.expect(organisation).to.exist
        done()
      })
      .catch(done)
    })
    it('getOrganisation', done => {
      this.note.getOrganisation()
      .then(organisation => {
        chai.expect(organisation).to.exist
        done()
      })
      .catch(done)
    })
    it('setOrganisation', done => {
      this.note.setOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('notes.BelongsToAuthorisedUserMixin', () => {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Notes.create()
        .then(note => {
          this.note = note
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createAuthorisedUser', done => {
      this.note.createAuthorisedUser()
      .then(authorisedUser => {
        chai.expect(authorisedUser).to.exist
        done()
      })
      .catch(done)
    })
    it('getAuthorisedUser', done => {
      this.note.getAuthorisedUser()
      .then(authorisedUser => {
        chai.expect(authorisedUser).to.exist
        done()
      })
      .catch(done)
    })
    it('setAuthorisedUser', done => {
      this.note.setAuthorisedUser()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('notes.BelongsToOwnerMixin', () => {
    before(done => {
      sequelize.sync({ force: true })
        .then(connection => {
          Notes.create()
            .then(note => {
              this.note = note
              done()
            })
            .catch(done)
        })
        .catch(done)
    })
    it('createOwner', done => {
      this.note.createOwner()
        .then(owner => {
          chai.expect(owner).to.exist
          done()
        })
        .catch(done)
    })
    it('getOwner', done => {
      this.note.getOwner()
        .then(owner => {
          chai.expect(owner).to.exist
          done()
        })
        .catch(done)
    })
    it('setOwner', done => {
      this.note.setOwner()
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
  })
  describe('notes.BelongsToAuthorisedUserMixin', () => {
    before(done => {
      sequelize.sync({ force: true })
        .then(connection => {
          Notes.create()
            .then(note => {
              this.note = note
              done()
            })
            .catch(done)
        })
        .catch(done)
    })
    it('createAuthorisedUser', done => {
      this.note.createAuthorisedUser()
        .then(authorisedUser => {
          chai.expect(authorisedUser).to.exist
          done()
        })
        .catch(done)
    })
    it('getAuthorisedUser', done => {
      this.note.getAuthorisedUser()
        .then(authorisedUser => {
          chai.expect(authorisedUser).to.exist
          done()
        })
        .catch(done)
    })
    it('setAuthorisedUser', done => {
      this.note.setAuthorisedUser()
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
  })
})

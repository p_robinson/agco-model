import * as chai from 'chai';
import { Systems, Organisations, Users, sequelize, Services, Notes, Locations, Commissions, Parts  } from '../model';
describe('Systems', () => {
  describe('Parts', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
      .catch(done)
    })
    before(done => {
      Organisations.create({
        name: 'Organisation'
      })
      .then(instance => {
        this.organisation = instance
        done()
      })
      .catch(done)
    })
    it('create a System with Parts and Notes', done => {
      this.organisation.createSystem({
        Parts: [{
          OrganisationId: this.organisation.id,
          Notes: [{
            OrganisationId: this.organisation.id
          }]
        }, {

        }]
      }, {
        include: [{
          model: Parts,
          include: [{
            model: Notes
          }]
        }]
      })
      .then(system => {
        chai.expect(system).to.exist
        .haveOwnProperty('Parts')

        chai.expect(system.Parts[0]).to.exist
        .haveOwnProperty('Notes')

        chai.expect(system.Parts[0].OrganisationId).to.equal(this.organisation.id)
        done()
      })
      .catch(done)
    })
  })
  describe('update', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Systems.create({
          Location: {
            address: '7 Haigh St'
          }
        }, {
          include: [{
            model: Locations
          }]
        })
        .then(instance => {
          this.system = instance
          done()
        })
        .catch(done)
      })
      it('Update Location', done => {
        this.system.update({
          Location: {
            address: '15 20 Laguna Drive'
          }
        }, {
          include: [{
            model: Locations
          }]
        })
        .then(instance => {
          chai.expect(instance.Location.address).to.equal('15 20 Laguna Drive')
          done()
        })
        .catch(done)
      })
    })
  })
  describe('create systems and nested associations', () => {
    before(done => {
      sequelize.sync()
      .then(() => {
         done()
      })
    })
     it('should create a simple system', done => {
       Systems.create()
       .then(system => {
         chai.expect(system).to.exist
         done()
       })
     })
     it('should create a system with location', done => {
       Systems.create({
         Location: {}
       }, {
         include: [{
           model: Locations
         }]
       })
       .then(instance => {
         chai.expect(instance).to.exist
         .haveOwnProperty('Location')
         done()
       })
       .catch(done)
     })
     it('should create a system with commission', done => {
       Systems.create({
         Commission: {}
       }, {
         include: [{
           model: Commissions
         }]
       })
       .then(instance => {
         chai.expect(instance).to.exist
         done()
       })
       .catch(done)
     })
     it('should create a system and notes', done => {
       Systems.create({
         Notes: [{}, {}]
       }, {
         include: [{
           model: Notes
         }]
       }).then(instance => {
         chai.expect(instance).to.exist
         .haveOwnProperty('Notes')
         chai.expect(instance.Notes.length).to.equal(2)
         done()
       })
       .catch(done)
     })
     it('should create an system with  owners', done => {
       Systems.create({
         Owners: [{
           name: 'Lucky Owner 3'
         }]
       },
         {
           include: [{
             model: Users,
             as: 'Owners'
           }]
         }
        )
       .then(system => {
         chai.expect(system).to.haveOwnProperty('Owners')
         done()
       }).catch(done)
     })
     it('should create a system with an organisation', done => {
       Systems.create({
         Organisation: {
           name: 'Organisation 0'
         }
       },
       {
         include: [{
         model: Organisations,
         as: 'Organisation'
       }]
     })
       .then(instance => {
         chai.expect(instance).to.haveOwnProperty('Organisation')
         done()
       }).catch(done)
    })
    it ('should create a system with organisation and users', done => {
      Systems.create({
        Organisation: {
          name: 'Organisation_2'
        },
        Owners: [{

        }]
      }, {
        include: [{
          model: Organisations,
          as: 'Organisation'
        }, {
          model: Users,
          as: 'Owners'
        }]
      }).then(instance => {
        chai.expect(instance).to
        .haveOwnProperty('Owners')
        .haveOwnProperty('Organisation')
        done()
      })
      .catch(done)
    })
    it('should create a system and services', done => {
      Systems.create({
        Services: [{}]
      }, {
        include: [{
          model: Services
        }]
      }).then(instance => {
        chai.expect(instance).to.haveOwnProperty('Services')
        done()
      })
    })
  })
  describe('Systems.BelongsToOrganisations', function () {
    before(done => {
      sequelize.sync({force: true}).then(() => {
        Promise.all([
          Organisations.create({
            name: 'Organisation 0'
          }),
          Systems.create()
        ]).then(values => {
          this.organisation = values[0]
          this.system = values[1]
          done()
        }).catch(done)
      }).catch(done)
    })
    it('should validate creation of system and organisation', done => {
      chai.expect(this).to.exist
      .haveOwnProperty('system')
      .haveOwnProperty('organisation')
      done()
    })
    it('should set and get organisation ', done => {
      this.system.setOrganisation(this.organisation)
      .then(instance => {
        chai.expect(instance).to.exist
        this.system.getOrganisation()
        .then(organisation => {
          chai.expect(organisation.id).to.equal(this.organisation.id)
          done()
        }).catch(done)
      }).catch(done)
    })
    it('should create an organisation', done => {
      this.system.createOrganisation({
        name: 'Organisation_1'
      }).then(instance => {
        this.organisation = instance
        done()
      })
    })
  })
  describe('Systems.BelongsToOwner', function () {
    before(done => {
      sequelize.sync({
        force: true
      })
      .then(() => {
        Systems.create()
        .then(system => {
          this.system = system
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('should check that system is created', done => {
      chai.expect(this.system).to.exist
      done()
    })
    it('should create a customer', done => {
      this.system.createOwner()
      .then(owner => {
        chai.expect(owner).to.exist
        done()
      })
      .catch(done)
    })
    it('Should Get Owners', done => {
      this.system.getOwners()
      .then(owners => {
        chai.expect(owners.length).to.equal(1)
        this.owner = owners[0]
        done()
      })
      .catch(done)
    })
    it('should remove Owner', done => {
      this.system.removeOwner(this.owner)
      .then(instance => {
        chai.expect(instance).to.exist
        this.system.countOwners()
        .then(count => {
          chai.expect(count).to.equal(0)
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('should set Owners', done => {
      this.system.setOwners(this.owner)
      .then(instance => {
        chai.expect(instance).to.exist
        this.system.countOwners()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('should has Owner', done => {
      this.system.hasOwner(this.owner)
      .then(has => {
        chai.expect(has).to.equal(true)
        done()
      }).catch(done)
    })
    it('should add an Owner', done => {
      Users.create()
      .then(user => {
        this.system.addOwner(user)
        .then(instance => {
          this.system.countOwners()
          .then(count => {
            chai.expect(count).to.equal(2)
            done()
          })
          .catch(done)
        })
        .catch(done)
      })
    })
  })
  describe('system.HasOneLocationMixIn', () => {
    before(done => {
      sequelize.sync({ force: true })
        .then(() => {
          Systems.create()
            .then(system => {
              this.system = system
              done()
            })
            .catch(done)
        })
        .catch(done)
    })
    it('createLocation', done => {
    this.system.createLocation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getLocation', done => {
    this.system.getLocation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setLocation', done => {
    this.system.setLocation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('system.HasManyCommissionMixIn', () => {
    before(done => {
      sequelize.sync({force: true})
       .then(() => {
         Systems.create()
         .then(system => {
           this.system = system
           done()
         })
         .catch(done)
       })
       .catch(done)
    })
    it('createCommission', done => {
      this.system.createCommission()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getCommissions', done => {
      this.system.getCommissions()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setCommissions', done => {
      this.system.setCommissions()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('system.HasManyNotesMixin', function () {
    before(done => {
      sequelize.sync({force: true})
       .then(() => {
         Systems.create()
         .then(system => {
           this.system = system
           done()
         })
         .catch(done)
       })
       .catch(done)
    })
    it('should create a note', done => {
      this.system.createNote()
       .then(note => {
         chai.expect(note.type).to.equal('SYSTEMS')
         done()
       })
       .catch(done)
    })
  })
  describe('organisation.HasManySystemsMixin', function() {
    before(done => {
      sequelize.sync({ force: true })
        .then(connection => {
          Organisations.create()
            .then(organisation => {
              this.organisation = organisation
              done()
            })
        }).catch(done)
    })
    it('createSystem', done => {
      this.organisation.createSystem()
        .then(instance => {
          this.location = instance
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('getSystem', done => {
      this.organisation.getSystems()
        .then(instance => {
          chai.expect(instance.length).to.equal(1)
          done()
        })
        .catch(done)
    })
    it('setSystems', done => {
      this.organisation.setSystems()
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('countSystems:0', done => {
      this.organisation.countSystems()
        .then(count => {
          chai.expect(count).to.equal(0)
          done()
        })
        .catch(done)
    })
    it('addSystem', done => {
      this.organisation.addSystem(this.location)
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
    })
    it('countSystem:1', done => {
      this.organisation.countSystems()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        })
        .catch(done)
    })
    it('removeSystem', done => {
      this.organisation.removeSystem(this.location)
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('hasSystem:false', done => {
      this.organisation.hasSystem(this.location)
        .then(has => {
          chai.expect(has).to.equal(false)
          done()
        })
        .catch(done)
    })
  })
})

import * as Sequelize from 'sequelize';
import { IModels } from './';
var Xero = require('xero-extension');

module.exports = function (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {

  var XeroOrganisations = sequelize.define('XeroOrganisations', {
    OrganisationId: {
      type: Sequelize.UUID,
      references: {
        model: 'Organisations',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING,
      allowNull: false
    },
    secret: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rsa_key: {
      type: DataTypes.BLOB,
      allowNull: false
    },
    ShortCode: {
      type: DataTypes.STRING
    },
    lastSync: {
      type: DataTypes.DATE
    },
    status: {
      type: DataTypes.ENUM(['valid', 'invalid', ]),
      defaultValue: 'invalid'
    },
    xeroConfiguration: {
      type: DataTypes.VIRTUAL,
      get: function() {
        return { key: this.key, secret: this.secret, rsa_key: this.rsa_key }
      }
    },
    profile: {
      type: Sequelize.BLOB,
      get: function() {
        return JSON.parse(this.getDataValue('profile'))
      },
      set: function(val) {
        this.setDataValue('profile', JSON.stringify(val))
      }
    }
  }
    , { instanceMethods: {
      createCustomer: function  (options = {}) {
      var xero = new Xero(this.xeroConfiguration)
  var contacts = xero.contacts
  return new Promise((resolve, reject) => {
        contacts.createOrUpdate(options)
          .then(contact => {
            resolve(contact)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    findById: function(id) {

      return new Promise((resolve, reject) => {
        if (!id) return reject(new Error('Xero ContactID must be set'));
        var xero = new Xero(this.xeroConfiguration)
        var contacts = xero.contacts
        contacts.findById(id)
          .then(contact => {
            resolve(contact)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
      sync: function (options = {}) {
      var xero = new Xero(this.xeroConfiguration)
  var organisation = xero.organisation
  return new Promise((resolve, reject) => {
        organisation.findAll(false)
          .then(organisation => {
            resolve(this.update({ profile: organisation, lastSync: Date.now(), status: 'valid', ShortCode: organisation.Organisations.Organisation.ShortCode }, { transaction: options.t }))
          })
          .catch(err => {
            reject(err)
          })
      })
    }
    }, hooks: {
    beforeCreate: function (xeroOrg, options, next) {

      var xero = new Xero(xeroOrg)
      var Organisation = xero.organisation
      Organisation.findAll(false)
        .then(organisation => {
          xeroOrg.profile = organisation;
          xeroOrg.lastSync = Date.now();
          xeroOrg.status = 'valid'
          xeroOrg.ShortCode = organisation.Organisations.Organisation.ShortCode
          next()
        })
        .catch(next)
    }
    } })
  return XeroOrganisations
}


module.exports = function (sequelize, DataTypes) {
  var Test = sequelize.define('Test', {
    UserId: {
      type: DataTypes.INTEGER
    },
    OrganisationId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function (models) {
        Test.belongsTo(models.AuthorisedUsers)
      }
    }
  })
  return Test
}

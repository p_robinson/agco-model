import { Users, XeroOrganisations, Customers, Organisations, XeroCustomers, sequelize } from '../model';
import { XeroOrganisation as XeroOrgTest } from './test.data';
import * as chai from 'chai';
import { XeroOrganisation } from './test.data';
describe('XeroOrganisations', function () {

  this.timeout(20000)
  describe('create', function () {
    this.timeout(10000)
    beforeEach(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Users.create()
        .then(instance => {
          this.user = instance
          done()
        })
      })
      .catch(done)
    })
    it('organisation', done => {

      XeroOrganisations.create(sequelize.Utils._.assign(
        XeroOrgTest,
        {
          Organisation: {
            name: 'Organisation'
          }
        }), {
        include: [{
          model: Organisations
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Organisation')
        done()
      })
      .catch(done)
    })

  })
  describe('HasManyXeroCustomersMixin', function () {
    this.timeout(10000)
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
      .catch(done)
    })
    before(done => {
      Organisations.create({
        name: 'Organisation',
        XeroOrganisation: XeroOrgTest
      }, {
        include: [{
          model: XeroOrganisations
        }]
      })
      .then(instance => {
        this.xeroOrganisation = instance.XeroOrganisation
        done()
      })
      .catch(done)
    })
    before(done => {
      Users.create({
        name: 'Paul Robinson',
        email: 'paul.robinson@kyndling.com',
        phone: '0499221986'
      }).then(instance => {
        this.user = instance
        done()
      })
      .catch(done)
    })
    it('create', done => {
      this.xeroOrganisation.createXeroCustomer({
        UserId: this.user.id,
        XeroProfileId: this.xeroOrganisation.OrganisationId
      })
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('Creating an Organisations Customer and adding to xero', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
      .catch(done)
    })
    before(done => {
      Organisations.create({
        XeroOrganisation: XeroOrganisation
      }, {
        include: [{
          model: XeroOrganisations
        }]
      })
      .then(instance => {
        this.organisation = instance
        done()
      })
      .catch(done)
    })
    it('create a customer and nested xero profile', done => {
      this.organisation.createCustomer({
        name: 'cd cd',
        XeroProfiles: [{
          XeroProfileId: this.organisation.id
        }]
      }, {
          include: [{
            model: XeroCustomers,
            as: 'XeroProfiles'
        }]
      })
      .then(instance => {
        this.instance = instance;
        chai.expect(instance.XeroProfiles[0].XeroProfileId).to.equal(this.organisation.id)
        done()
      })
      .catch(done)
    })
    it('check that user has been created', done => {
      this.organisation.getCustomers({
        include: [{
          model: XeroCustomers,
          as: 'XeroProfiles',
          where: {
               XeroProfileId: this.organisation.id
            }

        }]
      })
      .then(instance => {
        chai.expect(instance[0].XeroProfiles.length).to.equal(1)
        done()
      })
      .catch(done)
    })
    it('create a user from a xero ContactID', done => {
    this.organisation.createCustomer({
      XeroProfiles: [{
        XeroProfileId: this.organisation.id,
        ContactID: '565acaa9-e7f3-4fbf-80c3-16b081ddae10'
      }]
    }, {
      include: [{
        model: XeroCustomers,
        as: 'XeroProfiles',
      }]
    })
    .then(instance => {
      chai.expect(instance.XeroProfiles.length).to.equal(1)
      done()
    })
    .catch(done)
  })
  it('check that user has been created', done => {
    this.organisation.getCustomers({
      include: [{
        model: XeroCustomers,
        as: 'XeroProfiles',
        where: {
          XeroProfileId: this.organisation.id
        }

      }]
    })
      .then(instance => {
        chai.expect(instance.length).to.equal(2)
        done()
      })
      .catch(done)
    })
  })

})

import * as chai from 'chai';
import { XeroOrganisation } from './test.data'
import { Services, Organisations, Users, Systems, Notes, sequelize, XeroOrganisations, XeroCustomers, Commissions, Locations } from '../model';
describe('Services', () => {
  describe('Create a service and invoice a customer', function() {
    this.timeout(10000)
    before(done => {
      sequelize.sync({ force: true })
        .then(connection => {
          done()
        })
        .catch(done)
    })
    before(done => {
      Organisations.create({
        name: 'Organisation',
        XeroOrganisation: XeroOrganisation
      }, {
          include: [{
            model: XeroOrganisations
          }]
        })
        .then(instance => {
          this.organisation = instance
          done()
        })
        .catch(done)
    })
    before(done => {
      this.organisation.createCustomer({
        name: 'Paul Robinson',
        XeroProfiles: [{
          XeroProfileId: this.organisation.id
        }],
        CurrentSystems: [{
          OrganisationId: this.organisation.id,
          Commission: {
            OrganisationId: this.organisation.id
          },
          Location: {}
        }]
      }, {
          include: [{
            model: XeroCustomers,
            as: 'XeroProfiles'
          }, {
              model: Systems,
              as: 'CurrentSystems',
              include: [{
                model: Commissions
              }, {
                  model: Locations
                }]
            }]
        })
        .then(instance => {
          this.customer = instance
          done()
        })
    })
    it('check before tasks', done => {
      chai.expect(this.organisation).to.exist
      chai.expect(this.customer).to.exist

      chai.expect(this.customer).to.haveOwnProperty('XeroProfiles')
      chai.expect(this.customer).to.haveOwnProperty('CurrentSystems')
      done()
    })
    it('should create a service', done => {
      this.customer.CurrentSystems[0]
      .createService({
        OrganisationId: this.organisation.id,
        serviceData: {
          someData: 'someData'
        }
      })
      .then(service => {
        chai.expect(service.invoice).to.equal(true)
        done()
      }).catch(done)
    })
  })
  describe('Check Model Exists', () => {
    it('Services should exist', done => {
      chai.expect(Services).to.exist
      done()
    })
  })
  describe('Create Service', () => {
    before(done => {
      sequelize.sync({ force: true })
      .then(() => {
        done()
      })
      .catch(done)
    })
    it('Create a Simple Service', done => {
      Services.create()
      .then(service => {
        chai.expect(service).to.exist
        done()
      })
      .catch(done)
    })
    it('Create a Service and Organisation', done => {
      Services.create({
        Organisation: {
          name: 'Organisation'
        }
      }, {
        include: [{
          model: Organisations
        }]
      })
      .then(service => {
        chai.expect(service).to.haveOwnProperty('Organisation')
        done()
      })
      .catch(done)
    })
    it('Should Create a Service and Service Agent', done => {
      Services.create({
        ServiceAgent: {}
      }, {
        include: [{
          model: Users,
          as: 'ServiceAgent'
        }]
      })
      .then(instance => {
        chai.expect(instance).to.haveOwnProperty('ServiceAgent')
        done()
      })
      .catch(done)
    })
    it('Should create a service and System', done => {
      Services.create({
        System: {}
      }, {
        include: [{
          model: Systems,
          as: 'System'
        }]
      })
      .then(instance => {
        chai.expect(instance).to.haveOwnProperty('System')
        done()
      })
      .catch(done)
    })
    it('should create a service and notes', done => {
      Services.create({
        Notes: [{}]
      }, {
        include: [{
          model: Notes
        }]
      }).then(instance => {
        chai.expect(instance).to.haveOwnProperty('Notes')
        done()
      })
      .catch(done)
    })
  })
describe('service.HasManyNotesMixin', function () {
  before(done => {
    sequelize.sync({force: true})
     .then(() => {
       Services.create()
       .then(service => {
         this.service = service
         done()
       })
       .catch(done)
     })
     .catch(done)
  })
  it('should create a note', done => {
    this.service.createNote()
     .then(note => {
       chai.expect(note.type).to.equal('SERVICES')
       done()
     })
     .catch(done)
  })
})
})

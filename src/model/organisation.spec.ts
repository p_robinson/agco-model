import * as chai from 'chai';
import { Organisations, Users, Notes, Systems, sequelize, Customers, Locations } from '../model';
describe('Organisations', () => {
  describe('create Organisation include nested', ()=> {
    before(done => {
      sequelize.sync({force: true})
      .then(()=> {
        done()
      })
      .catch(done)
    })
    it('create organisation, system and location', done => {
      Organisations.create({
        Systems: [{
          Location: {}
        }]
      }, {
        include: [{
          model: Systems,
          include: [{
            model:Locations
          }]
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Systems')
        chai.expect(instance.Systems[0]).to.exist
        .haveOwnProperty('Location')
        done()
      })
      .catch(done)
    })
    it('should create an organisation with nested notes', done => {
      Organisations.create({
        name: 'Organisation_1',
        Notes: [{}, {}]
      }, {
        include: [{
          model: Notes
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Notes')
        chai.expect(instance.Notes.length).to.equal(2)
        done()
      })
      .catch(done)
    })
    it('should create an organisation with nested locations', done => {
    Organisations.create({
      name: 'Organisation_1',
      Locations: [{}, {}]
    }, {
        include: [{
          model: Locations
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
          .haveOwnProperty('Locations')
        chai.expect(instance.Locations.length).to.equal(2)
        done()
      })
      .catch(done)
    })
    it('should create an organisation with nested systems', done => {
      Organisations.create({
        name: 'Organisation_2',
        Systems: [{}, {}]
      }, {
        include: [{
          model: Systems
        }]
      }).then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Systems')
        chai.expect(instance.Systems.length).to.equal(2)
        done()
      })
      .catch(done)
    })
    it('should create Organisation with nested owner', done => {
      Organisations.create({
        name: 'Organisation0',
        Owner: {
          name: 'Lucky User'
        }
      }, {
        include: [{model: Users, as: 'Owner'}]
      }).then(instance => {
        chai.expect(instance).to.haveOwnProperty('Owner')
        done()
      })
      .catch(done)
    })
    it('should create Organisation with customer', done => {
      Organisations.create({
        name: 'Organisation 1',
        Customers: [
          {name: 'Lucky User 2'}
        ]
      }, {
        include: [{
            model: Users,
            as: 'Customers'
          }]
        })
      .then(instance => {
        chai.expect(instance).to.haveOwnProperty('Customers')
        done()
      })
      .catch(done)
    })
    it('should create an organisation and authorised Users', done => {
       Organisations.create({
         name: 'Organisation 2',
         AuthdUsers: [{
             name: 'Lucky User 3'
           }]
       }, {
         include: [{
           model: Users,
           as: 'AuthdUsers'}]
       })
       .then(instance => {
         chai.expect(instance).to.haveOwnProperty('AuthdUsers')
         done()
       })
    })
    it('should create Organisation and Owner', done => {
      Organisations.create({
        name: 'Organisation 3',
        Owner: {
          name: 'Lucky Owner'
        }
      }, {
        include: [{
          model: Users,
          as: 'Owner'
        }]
      }).then(instance => {

        chai.expect(instance).to.haveOwnProperty('Owner')
        done()
      })
      .catch(done)
    })
    it('should create an organisation', done => {
      Organisations.create({
        name: 'organisation 4'
      })
      .then(organisation => {
        this.organisation = organisation
        done()
      })
      .catch(done)
    })
    it('should create a customer', done => {
      this.organisation.createCustomer({
        name: 'Lucky User 4'
      })
      .then(customer => {
        chai.expect(customer).to.exist
        this.customer = customer
        this.organisation.countCustomers()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        })
        .catch(done)
      })
    it('should remove a customer', done => {
      this.organisation.removeCustomer(this.customer)
      .then(instance => {
         this.organisation.countCustomer()
         .then(count => {
           chai.expect(count).to.equal(0)
           done()
         }).catch(done)
      }).catch(done)
    })
    it('should add a customer', done => {
      this.organisation.addCustomer(this.customer)
      .then(instance => {
        this.organisation.countCustomers()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        }).catch(done)
      }).catch(done)
    })
    it('should set a customer', done => {
      this.organisation.removeCustomer(this.customer)
      .then(instance => {
        this.organisation.countCustomers()
        .then(count => {
          chai.expect(count).to.equal(0)
          this.organisation.setCustomer(this.customer)
          .then(insatnce => {
            this.organisation.countCustomers()
            .then(count => {
              chai.expect(count).to.equal(1)
              done()
            })
          })
        })
      })
    })
    it('should prove that organisation has a customer', done => {
      this.organisation.hasCustomer(this.customer)
      .then(has => {
        chai.expect(has).to.equal(true)
        done()
      })
    })

    })
  })
  describe('organisation.HasManyLocationsMixin', function () {
    before(done => {
      sequelize.sync({ force: true })
        .then(connection => {
          Organisations.create()
            .then(organisation => {
              this.organisation = organisation
              done()
            })
        }).catch(done)
    })
    it('createLocation', done => {
      this.organisation.createLocation()
        .then(instance => {
          this.location = instance
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('getLocation', done => {
      this.organisation.getLocations()
        .then(instance => {
          chai.expect(instance.length).to.equal(1)
          done()
        })
        .catch(done)
    })
    it('setLocations', done => {
      this.organisation.setLocations()
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('countLocations:0', done => {
      this.organisation.countLocations()
      .then(count => {
        chai.expect(count).to.equal(0)
        done()
      })
      .catch(done)
    })
    it('addLocation', done => {
      this.organisation.addLocation(this.location)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
    })
    it('countLocation:1', done => {
      this.organisation.countLocations()
      .then(count => {
        chai.expect(count).to.equal(1)
        done()
      })
      .catch(done)
    })
    it('removeLocation', done => {
      this.organisation.removeLocation(this.location)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('hasLocation:false', done => {
      this.organisation.hasLocation(this.location)
      .then(has => {
        chai.expect(has).to.equal(false)
        done()
      })
      .catch(done)
    })
  })
  describe('HasManySystems', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Organisations.create()
        .then(instance => {
          this.organisation = instance
          done()
        })
      })
    })
    it('createSystem', done => {
      this.organisation.createSystem({
        Location: {}
      }, {
        include: [{
          model: Locations
        }]
      })
      .then(system => {
        chai.expect(system).to
        .haveOwnProperty('Location')
        done()
      })
      .catch(done)
    })
  })
  describe('organisation.HasManyCommissionsMixin', function() {
    before(done => {
      sequelize.sync({ force: true })
        .then(connection => {
          Organisations.create()
            .then(organisation => {
              this.organisation = organisation
              done()
            })
        }).catch(done)
    })
    it('createCommission', done => {
      this.organisation.createCommission()
        .then(instance => {
          this.location = instance
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('getCommission', done => {
      this.organisation.getCommissions()
        .then(instance => {
          chai.expect(instance.length).to.equal(1)
          done()
        })
        .catch(done)
    })
    it('setCommissions', done => {
      this.organisation.setCommissions()
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('countCommissions:0', done => {
      this.organisation.countCommissions()
        .then(count => {
          chai.expect(count).to.equal(0)
          done()
        })
        .catch(done)
    })
    it('addCommission', done => {
      this.organisation.addCommission(this.location)
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
    })
    it('countCommission:1', done => {
      this.organisation.countCommissions()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        })
        .catch(done)
    })
    it('removeCommission', done => {
      this.organisation.removeCommission(this.location)
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
    })
    it('hasCommission:false', done => {
      this.organisation.hasCommission(this.location)
        .then(has => {
          chai.expect(has).to.equal(false)
          done()
        })
        .catch(done)
    })
  })
})

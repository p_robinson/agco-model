import * as chai from 'chai';
import { sequelize, Users, LocalLogin, Organisations, AuthorisedUsers, Services, Notes, XeroCustomers, XeroOrganisations } from '../model';
import {XeroOrganisation as XeroOrgTest } from './test.data';

describe('AgcoModel:User', () => {
  before(done => {
    sequelize.sync({ force: true })
    .then(() => {
      done()
    })
    .catch(done)
  })
  it('Should Create a Plain User', done => {
    Users.create()
    .then(user => {
      chai.expect(user).to.exist
      done()
    })
    .catch(done)
  })
  it( 'should create nested customer Notes', done => {
    Users.create({
      CustomerNotes: [{}, {}]
    }, {
      include: [{
        model: Notes,
        as: 'CustomerNotes'
      }]
    })
    .then(instance => {
      chai.expect(instance).to.exist
      .haveOwnProperty('CustomerNotes')

      chai.expect(instance.CustomerNotes.length).to.equal(2)
      done()
    })
    .catch(done)
  })
  it('should create a user and service', done => {
    Users.create({
      Services: [{}]
    }, {
      include: [{
        model: Services
      }]
    })
    .then(instance => {
      chai.expect(instance).to.haveOwnProperty('Services')
      done()
    })
    .catch(done)
  })
  it('should create a user and Owners notes', done => {
    Users.create({
      OwnerNotes: [{}, {}]
    }, {
      include: [{
        model: Notes,
        as: 'OwnerNotes'
      }]
    })
    .then(instance => {
      chai.expect(instance).to.haveOwnProperty('OwnerNotes')
      chai.expect(instance.OwnerNotes.length).to.equal(2)
      done()
    })
    .catch(done)
  })
  it('should create a user and Authorised notes', done => {
    Users.create({
      AuthorisedUserNotes: [{}, {}]
    }, {
        include: [{
          model: Notes,
          as: 'AuthorisedUserNotes'
        }]
      })
      .then(instance => {
      chai.expect(instance).to.haveOwnProperty('AuthorisedUserNotes')
        chai.expect(instance.AuthorisedUserNotes.length).to.equal(2)
        done()
      })
      .catch(done)
  })
  it('Should Create a User with a Login ', done => {
    Users.create({
      LocalLogin: {
        email: 'paul.robinson@kyndling.com',
        password: 'password'
      }
    },
    {
      include: [{model: LocalLogin, attributes: ['email', 'password']}]
    })
    .then(user => {
      chai.expect(user).to.exist
      .haveOwnProperty('LocalLogin')
      done()
    }).catch(done)
  })
  it('Should Create a User With an Owned Organisation', done => {

    Users.create({
      OwnedOrganisations: [
      {name: 'Organisation1'}
      ]
    },
    {
      include: [{ model: Organisations, as: 'OwnedOrganisations', attributes: ['name'] }]
    })
    .then(user => {
      done()
    })
    .catch(done)
  })
  it('should create a user with Organisation as authorised Organisations', done => {

    Users.create({
      AuthorisedOrganisations: [{
        name: 'Organisation2'

      }]
    },
    { include: [{ model: Organisations, as: 'AuthorisedOrganisations', attributes: ['name'] }] }
    )
    .then(user => {
      chai.expect(user).to.haveOwnProperty('AuthorisedOrganisations')
      done()
    }).catch(done)
  })
  it('should also exist', function(done) {
    Users.create()
    .then(user => {
      chai.expect(user).to.exist
      done()
    }).catch(done)
  })
  describe('user.HasManyServicesMixin', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(() => {
        Users.create()
        .then(user => {
          this.user = user
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createService', done => {
      this.user.createService()
      .then(service => {
        this.service = service
        chai.expect(service).to.exist
        done()
      })
      .catch(done)
    })
    it('removeService', done => {
      this.user.removeService(this.service)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('hasService:false', done => {
      this.user.hasService(this.service)
      .then(has => {
        chai.expect(has).to.exist
        done()
      })
      .catch(done)
    })
    it('addService', done => {
      this.user.addService(this.service)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('hasService:true', done => {
      this.user.hasService(this.service)
      .then(has => {
        chai.expect(has).to.exist
        done()
      })
      .catch(done)
    })
    it('setService', done => {
      this.user.createService()
      .then(service => {
        this.user.setServices(service)
        .then(instance => {
          chai.expect(instance).to.exist
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('countServices', done => {
      this.user.countServices()
      .then(count => {
        chai.expect(count).to.equal(1)
        done()
      })
      .catch(done)
    })
  })
  describe('user.HasManyOrganisationsMixin', function () {
    describe('createOrganisation', () => {
      before(done => {
        Users.create()
        .then(instance => {
          this.user = instance
          done()
        }).catch(done)
      })
      it('should create an Organisation', done => {
        this.user.createOwnedOrganisation({name: 'Organisation'})
        .then(organisation => {
          this.organisation =  organisation
          chai.expect(organisation).to.exist
          chai.expect(organisation.UserId).to.exist
          done()
        })
        .catch(done)
      })
      it('should remove an organisation', done => {
        this.user.removeOwnedOrganisation(this.organisation)
        .then(organisation => {
          chai.expect(organisation).to.exist
          chai.expect(organisation.UserId).to.not.exist
          done()
        })
        .catch(done)
      })
      it('should add an organisation ans count organisations', done => {
        this.organisation.reload().then(organisation => {
          this.user.addOwnedOrganisation(organisation)
          .then(user => {
            user.countOwnedOrganisations()
            .then(amount => {
              chai.expect(amount).to.equal(1)
              done()
            })
            .catch(done)
          })
          .catch(done)
        })
        it('should set an organisation to a user', done => {
          this.user.removeOwnedOrganisation(this.organisation)
          .then(instance => {
            user.countOwnedOrganisations()
            .then(count => {
              chai.expect(count).to.equal(0)
              this.user.setOwnedOrganisation(this.organisation)
              .then(instance => {
                this.user.countOwnedOrganisations()
                .then(count => {
                  chai.expect(count).to.equal(1)
                  done()
                })
              })
            })
          })
        })
      })
    })
  describe('user.HasOneLocalLogin', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(() => {
        Users.create()
        .then(user => {
          this.user = user
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('should create a local login', done => {
      this.user.createLocalLogin({
        email: 'paul.robinson@kyndling.com',
        password: 'password'
      })
      .then(loginDetails => {
        chai.expect(loginDetails).to.exist
        done()
      })
      .catch(done)
    })
    it('get a local login', done => {
      this.user.getLocalLogin()
      .then(localLogin => {
        chai.expect(localLogin).to.exist
        done()
      }).catch(done)
    })
    it('should set a local login', done => {
      LocalLogin.create({
        email: 'user@email.com',
        password: 'password'
      })
      .then(localLogin => {
        this.user.setLocalLogin(localLogin)
        .then(localLogin => {
          chai.expect(localLogin.UserId).to.equal(this.user.id)
          done()
        })
      })
    })
  })
  describe('user.HasManyCustomerNotesMixin', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(() => {
        Users.create().then(user => {
          this.user = user
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('should create a customer Note', done => {
      this.user.createCustomerNote()
      .then(customerNote => {
        chai.expect(customerNote.type).to.equal('CUSTOMERS')
        done()
      })
      .catch(done)
    })
  })
  describe('user.BelongsToManyOrganisations as AuthorisedUsers', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(() => {
        Promise.all([
          Users.create(),
          Organisations.create({name: 'Organisation'})
          ])
        .then(value => {
          this.user = value[0]
          this.organisation = value[1]
          done()
        })
        .catch(done)
      })
    })
    it('should add and count an organisation to a user ', done => {
      this.user.addAuthorisedOrganisation(this.organisation)
      .then(instance => {
        this.user.countAuthorisedOrganisations()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('remove and count an authorised organisation', done => {
      this.user.removeAuthorisedOrganisation(this.organisation)
      .then(instance => {
        this.user.countAuthorisedOrganisations()
        .then(count => {
          chai.expect(count).to.equal(0)
          done()
        }).catch(done)
      }).catch(done)
    })
    it('should set and count an authorised organisation', done => {
      this.user.setAuthorisedOrganisations(this.organisation)
      .then(insatance => {
        this.user.countAuthorisedOrganisations()
        .then(count => {
          chai.expect(count).to.equal(1)
          done()
        }).catch(done)
      }).catch(done)
    })
    it('should have organisation as Authorised User', done => {
      this.user.hasAuthorisedOrganisation(this.organisation)
      .then(instance => {
        chai.expect(instance).to.be.true
        done()
      })
    })
    describe('user.BelongsToManyCustomers', () => {
      before(done => {
        sequelize.sync({force: true})
        .then(() => {
          Users.create()
          .then(user => {
            this.user = user
            done()
          })
        })
        .catch(done)
      })
      it('should create Organisation as Customer Account', done => {
        this.user.createCustomerAccount({
          name: 'organisation'
        })
        .then(instance => {
          chai.expect(instance).to.exist
          this.organisation = instance
          done()
        })
      })
      it('remove and count an customer account', done => {
        this.user.removeCustomerAccount(this.organisation)
        .then(instance => {
          this.user.countCustomerAccounts()
          .then(count => {
            chai.expect(count).to.equal(0)
            done()
          }).catch(done)
        }).catch(done)
      })
      it('should set and count an customer account', done => {
        this.user.setCustomerAccounts(this.organisation)
        .then(insatance => {
          this.user.countCustomerAccounts()
          .then(count => {
            chai.expect(count).to.equal(1)
            done()
          }).catch(done)
        }).catch(done)
      })
      it('should have customer account as customer User', done => {
        this.user.hasCustomerAccount(this.organisation)
        .then(instance => {
          chai.expect(instance).to.be.true
          done()
        })
        .catch(done)
      })
    })
  })
})
  describe('HasManyXeroCustomers', function () {
    this.timeout(10000)
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
      .catch(done)
    })
    before(done => {
      Organisations.create({
        name: 'Organisation',
        XeroOrganisation: XeroOrgTest
      }, {
        include: [{
          model: XeroOrganisations,
        }]
      }).then(instance => {
        this.organisation = instance
        done()
      })
      .catch(done)
    })
    before(done => {
      Users.create({
        name: 'Paul Robinson'
      })
      .then(instance => {
        this.user = instance
        done()
      })
    })
    before(done => {
      XeroCustomers.bulkCreate([{
        XeroProfileId: this.organisation.id,
        ContactID: 'CONTACTID1'
      }, {
        XeroProfileId: this.organisation.id,
        ContactID: 'CONTACTID2'
      }, {
        XeroProfileId: this.organisation.id,
        ContactID: 'CONTACTID3'
      }, {
        XeroProfileId: this.organisation.id,
        ContactID: 'CONTACTID4'
      }])
      .then(instance => {
        this.xeroProfiles = instance
        done()
      })
      .catch(done)
    })
    it('createXeroProfile', done => {
      this.user.createXeroProfile({

        XeroProfileId: this.organisation.id,
      })
      .then(instance => {
        this.xeroProfile = instance
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getXeroProfiles', done => {
      this.user.getXeroProfiles()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('removeXeroProfile', done => {
      this.user.removeXeroProfile(this.xeroProfile)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('countXeroProfiles', done => {
      this.user.countXeroProfiles()
      .then(count => {
        chai.expect(count).to.equal(0)
        done()
      })
      .catch(done)
    })
    it('hasXeroProfile', done => {
      this.user.hasXeroProfile(this.xeroProfile)
      .then(has => {
        chai.expect(has).to.equal(false)
        done()
      })
      .catch(done)
    })
    it.skip('addXeroProfile', done => {
      this.user.addXeroProfile(this.xeroProfiles[0])
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('addXeroProfiles', done => {
      this.user.addXeroProfiles(this.xeroProfiles)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('hasXeroProfiles', done => {
      this.user.hasXeroProfiles(this.xeroProfiles)
      .then(has => {
        chai.expect(has).to.equal(true)
        done()
      })
      .catch(done)
    })
    it('removeXeroProfiles', done => {
      this.user.removeXeroProfiles(this.xeroProfiles)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setXeroProfiles', done => {
      this.user.setXeroProfiles(this.xeroProfiles)
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('countXeroProfiles', done => {
      this.user.countXeroProfiles()
      .then(count => {
        chai.expect(count).to.equal(4)
        done()
      })
      .catch(done)
    })
  })
})

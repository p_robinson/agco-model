import { Sequelize, DataTypes, Instance, Model, HasOneGetAssociationMixin, HasOneSetAssociationMixin, HasOneCreateAssociationMixin } from 'sequelize';
import { IModels } from './';
import { XeroCustomerInstance, XeroCustomerAttributes } from './XeroCustomers'

module.exports = function (sequelize: Sequelize, DataTypes: DataTypes) {
  var Customers = <CustomerModel> sequelize.define('Customers', {
    UserId: {
      type: DataTypes.UUID,
      unique: 'Customers',
      primaryKey: true,
      references: {
        model: 'Users',
        key: 'id'
      }
    },
    OrganisationId: {
      type: DataTypes.UUID,
      unique: 'Customers',
      primaryKey: true,
      references: {
        model: 'Organisations',
        key: 'id'
      }
    }
  }, {
    classMethods: {
      associate: function (models: IModels) {

      }
    }
  })
  return Customers
}
export interface CustomerAttributes {}
export interface CustomerInstance extends Instance<CustomerInstance, CustomerAttributes> {
  getXeroProfile: HasOneGetAssociationMixin<XeroCustomerInstance>;
  setXeroProfile: HasOneSetAssociationMixin<XeroCustomerInstance, number>;
  createXeroProfile: HasOneCreateAssociationMixin<XeroCustomerAttributes>;
}
export interface CustomerModel extends Model<CustomerInstance, CustomerAttributes> {}

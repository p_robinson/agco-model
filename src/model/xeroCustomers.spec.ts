import * as chai from 'chai';
import { sequelize, XeroCustomers, XeroOrganisations, Organisations, Users } from '../model';
import { XeroOrganisation } from './test.data';
describe('XeroCustomers', () => {
  describe('invoice', function () {
    this.timeout(10000)
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        done()
      })
    })
    before(done => {
      Organisations.create({
        name: 'Organisation',
        XeroOrganisation: XeroOrganisation
      }, {
        include: [{
          model: XeroOrganisations
        }]
      })
      .then(instance => {
        this.organisation = instance
        done()
      })
    })
    before(done => {
      this.organisation.createCustomer({
        name: 'Paul Robinson',
        XeroProfiles: [{
          XeroProfileId: this.organisation.id
        }]
      }, {
        include: [{
          model: XeroCustomers,
          as: 'XeroProfiles'
        }]
      })
      .then(instance => {
        this.customer = instance
        done()
      })
    })
    it('check befores are successful', done => {
      chai.expect(this.customer).to.exist
      chai.expect(this.organisation).to.exist
      done()
    })
    it('invoice customer', done => {
      this.customer.XeroProfiles[0].invoice(
        '2011-07-20',
        [{ Description: 'Services As Agreed', Quantity: 4, UnitAmount: 100.00, AccountCode: 200 }]
      )
      .then(invoice => {
        console.log(invoice)
        done()
      })
      .catch(done)
    })
  })
})

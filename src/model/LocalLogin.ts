import {attributes, instanceMethods, classMethods, hooks } from 'prUser'
import { BelongsToGetAssociationMixin, BelongsToCreateAssociationMixin, BelongsToSetAssociationMixin } from 'sequelize';
import { IModels } from './';
import { UserAttributes, UserInstance } from './Users'
import * as Sequelize from 'sequelize'
module.exports =  function (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
  sequelize.Utils._.extend(classMethods, {
    associate: function (models: IModels): void {
      LocalLogin.belongsTo(models.Users)
    }
  })

  var LocalLogin = <LocalLoginModel>sequelize.define('LocalLogin',
    attributes.attributes,
    {
    instanceMethods,
    classMethods,
    hooks
  })
  return LocalLogin
}
export interface LocalLoginAttributes extends attributes.IAttributes {
  user?: UserAttributes
}
export interface LocalLoginInstance extends Sequelize.Instance<LocalLoginInstance, LocalLoginAttributes>, instanceMethods.IInstanceMethods<LocalLoginInstance> {
  getUser: BelongsToGetAssociationMixin<UserInstance>,
  createUser: BelongsToCreateAssociationMixin<UserInstance>,
  setUser: BelongsToSetAssociationMixin<UserInstance, number>
}
export interface LocalLoginModel extends Sequelize.Model<LocalLoginInstance, LocalLoginAttributes>, classMethods.IClassMethods {}

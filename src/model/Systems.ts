import { Sequelize, DataTypes, Instance, Model, BelongsToGetAssociationMixin, BelongsToSetAssociationMixin, BelongsToCreateAssociationMixin, BelongsToManyGetAssociationsMixin, BelongsToManySetAssociationsMixin, BelongsToManyAddAssociationsMixin, BelongsToManyAddAssociationMixin, BelongsToManyCreateAssociationMixin, BelongsToManyRemoveAssociationMixin, BelongsToManyRemoveAssociationsMixin, BelongsToManyHasAssociationMixin, BelongsToManyHasAssociationsMixin, BelongsToManyCountAssociationsMixin } from 'sequelize';
import { IModels } from './';
import { OrganisationInstance, OrganisationAttributes } from './Organisations';
import { CustomerInstance, CustomerAttributes } from './Customers';
module.exports = function (sequelize: Sequelize, DataTypes: DataTypes) {
  var Systems = <SystemModel>sequelize.define('Systems', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    'sold': {
      //legacy
      type: DataTypes.VIRTUAL,
      get: function () {
        return 0
      }
    },
    'ozzikleenSystems_serial': {
      //Legacy
      type: DataTypes.VIRTUAL,
      get: function () {
        return this.getDataValue('serial')
      }
    },
    serial: DataTypes.STRING,
    selling: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    serviceInterval: {
      type: DataTypes.INTEGER,
      defaultValue: 90
    },
    commission: DataTypes.DATE,
    lastService: DataTypes.DATE,
    nextService: DataTypes.DATE,
    service: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    status: DataTypes.ENUM(['ORDERED', 'DELIVERED','AWAITING_COMMISSION', 'COMMISSIONED']),
    type: DataTypes.ENUM(['OZZIKLEEN', 'FUJICLEAN', 'KRYSTALKLEAR']),
    days: {
      type: DataTypes.VIRTUAL,
      get: function () {
        if (this.lastService instanceof Date) return Math.floor((new Date() - this.lastService) / (1000*60*60*24))
        return undefined
      }
    }
  })
  return Systems
}
export interface SystemAttributes {
  lastService?: string;
  nextService?: string;
  status?: string;
  type?: string;
}
export interface SystemInstance extends Instance<SystemInstance, SystemAttributes> {
  getOrganisation: BelongsToGetAssociationMixin<OrganisationInstance>;
  setOrganisation: BelongsToSetAssociationMixin<OrganisationInstance, number>;
  createOrganisation: BelongsToCreateAssociationMixin<OrganisationAttributes>;

  getCustomers: BelongsToManyGetAssociationsMixin<CustomerInstance>;
  setCustomers: BelongsToManySetAssociationsMixin<CustomerInstance, number, CustomerAttributes>;
  addCustomers: BelongsToManyAddAssociationsMixin<CustomerInstance, number, CustomerAttributes>;
  addCustomer: BelongsToManyAddAssociationMixin<CustomerInstance, number, CustomerAttributes>;
  createCustomer: BelongsToManyCreateAssociationMixin<CustomerAttributes, CustomerAttributes>;
  removeCustomer: BelongsToManyRemoveAssociationMixin<CustomerInstance, number>;
  removeCustomers: BelongsToManyRemoveAssociationsMixin<CustomerInstance, number>;
  hasCustomer: BelongsToManyHasAssociationMixin<CustomerInstance, number>;
  hasCustomers: BelongsToManyHasAssociationsMixin<CustomerInstance, number>;
  countCustomers: BelongsToManyCountAssociationsMixin;


}
export interface SystemModel extends Model<SystemInstance, SystemAttributes> {}

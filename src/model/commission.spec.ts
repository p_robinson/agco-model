import { Commissions, Users, Organisations, Systems, sequelize } from '../model';
import * as chai from 'chai';

describe('Commissions', () => {
  describe('create with nested', () => {
    it('Organisation', done => {
      Commissions.create({
        Organisation: {
          name: 'Organisation'
        }
      }, {
        include: [{
          model: Organisations
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('Organisation')
        done()
      })
      .catch(done)
    })
    it('System', done => {
      Commissions.create({
        System: {}
      }, {
        include: [{
          model: Systems
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('System')
        done()
      })
      .catch(done)
    })
    it('AuthorisedUser', done => {
      Commissions.create({
        AuthorisedUser: {}
      }, {
        include: [{
          model: Users,
          as: 'AuthorisedUser'
        }]
      })
      .then(instance => {
        chai.expect(instance).to.exist
        .haveOwnProperty('AuthorisedUser')
        done()
      })
      .catch(done)
    })
  })
  describe('BelongsToOrganisationMixin', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Commissions.create()
        .then(commission => {
          this.commission = commission
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createOrganisation', done => {
      this.commission.createOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getOrganisation', done => {
      this.commission.getOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setOrganisation', done => {
      this.commission.setOrganisation()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('BelongsToAuthorisedUserMixin', function () {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Commissions.create()
        .then(commission => {
          this.commission = commission
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createAuthorisedUser', done => {
      this.commission.createAuthorisedUser()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getAuthorisedUser', done => {
      this.commission.getAuthorisedUser()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setAuthorisedUser', done => {
      this.commission.setAuthorisedUser()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('BelongsToSystemMixin', function() {
    before(done => {
      sequelize.sync({force: true})
      .then(connection => {
        Commissions.create()
        .then(commission => {
          this.commission = commission
          done()
        })
        .catch(done)
      })
      .catch(done)
    })
    it('createSystem', done => {
      this.commission.createSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('getSystem', done => {
      this.commission.getSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
    it('setSystem', done => {
      this.commission.setSystem()
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
})
